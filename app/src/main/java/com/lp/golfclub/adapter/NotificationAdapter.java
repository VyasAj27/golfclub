package com.lp.golfclub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.fragments.NotificationDetailFragment;
import com.lp.golfclub.model.NotificationList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private List<NotificationList> moviesList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rlMain)
        RelativeLayout rlMain;

        @BindView(R.id.tvDate)
        AppCompatTextView tvDate;

        @BindView(R.id.tvTitle)
        AppCompatTextView tvTitle;

        @BindView(R.id.tvDetail)
        AppCompatTextView tvDetail;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

            rlMain = view.findViewById(R.id.rlMain);
        }
    }

    public NotificationAdapter(List<NotificationList> moviesList , Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        NotificationList notificationList = moviesList.get(position);

        holder.tvDate.setText(notificationList.getTodayDate());
        holder.tvTitle.setText(notificationList.getTitle());
        holder.tvDetail.setText(notificationList.getDescription());

        holder.rlMain.setOnClickListener(view -> ((MainActivity)activity).loadFragment(new NotificationDetailFragment(notificationList)));
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
