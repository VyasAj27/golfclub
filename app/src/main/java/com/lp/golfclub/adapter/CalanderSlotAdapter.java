package com.lp.golfclub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.model.SlotTimeResponseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalanderSlotAdapter extends RecyclerView.Adapter<CalanderSlotAdapter.MyViewHolder> {

    private List<SlotTimeResponseModel> slotTimes;
    private Activity activity;
    private List<SlotTimeResponseModel> currentSelectedSlots;
    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private Calendar calendar;

    public void setCalanderDate(Calendar calendar) {
        this.calendar = calendar;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rlMain)
        RelativeLayout rlMain;

        @BindView(R.id.tvTime)
        AppCompatTextView tvTime;

        @BindView(R.id.tvAvailableSlots)
        AppCompatTextView tvAvailableSlots;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            rlMain = view.findViewById(R.id.rlMain);
        }
    }

    public CalanderSlotAdapter(List<SlotTimeResponseModel> slotTimes, Activity activity) {
        this.slotTimes = slotTimes;
        this.activity = activity;
        currentSelectedSlots = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_slot, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        SlotTimeResponseModel slotTime = slotTimes.get(position);

        holder.tvTime.setText(slotTime.getSlotTitle());

        if (slotTime.getBookingAllowed() == 1) {
            holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.register_bg));
            holder.tvTime.setTextColor(ContextCompat.getColor(activity, R.color.lightGrey));
        }
        if (slotTime.getAlreadyBooked() == 1) {
            holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.booked_border));
            holder.tvTime.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
        }
        if (slotTime.getBookingAllowed().equals("NA")) {
            holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.not_available_border));
            holder.tvTime.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
        }

        if (slotTime.isSelected()) {
            holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.available_border));
            holder.tvTime.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
        }

        holder.tvAvailableSlots.setText("Available: "+ slotTime.getRemainingBooking());

        holder.rlMain.setOnClickListener(view -> {
            boolean isPassed = false;
            if (dateFormat.format(calendar.getTime()).equals(dateFormat.format(Calendar.getInstance().getTime()))) {
                isPassed = checkCurrentTimePassed(slotTime.getSlotTitle());
            }
            if (isPassed) {
                Toast.makeText(activity, "Slot time is already passed.", Toast.LENGTH_SHORT).show();
            } else {
                currentSelectedSlots.clear();
                removeSelected();
                if (slotTime.getBookingAllowed() == 1) {
                    if (slotTime.isSelected()) {
                        slotTime.setSelected(false);

                        if (slotTime.getAlreadyBooked() == 1) {
                            holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.booked_border));
                            holder.tvTime.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
                        } else {
                            holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.register_bg));
                            holder.tvTime.setTextColor(ContextCompat.getColor(activity, R.color.lightGrey));
                        }
                        currentSelectedSlots.remove(slotTime);
                    } else {
                        slotTime.setSelected(true);
                        holder.rlMain.setBackground(ContextCompat.getDrawable(activity, R.drawable.available_border));
                        holder.tvTime.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
                        currentSelectedSlots.add(slotTime);
                    }
                } else {
                    Toast.makeText(activity, "Slot already Booked.", Toast.LENGTH_SHORT).show();
                }
                notifyDataSetChanged();
            }
        });
    }

    private void removeSelected() {
        for (int i = 0; i < slotTimes.size(); i++) {
            slotTimes.get(i).setSelected(false);
        }
    }

    private boolean checkCurrentTimePassed(String slotTitle) {
        try {
            String[] split = slotTitle.split(" - ");
            Date start = sdf.parse(split[0]);

            Date current = sdf.parse(sdf.format(Calendar.getInstance().getTime()));

            if (start.before(current)) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<SlotTimeResponseModel> getCurrentSelectedTabs() {
        return currentSelectedSlots;
    }

    @Override
    public int getItemCount() {
        return slotTimes.size();
    }
}
