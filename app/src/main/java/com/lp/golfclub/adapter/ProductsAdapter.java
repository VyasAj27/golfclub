package com.lp.golfclub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.fragments.BookingCalenderFragment;
import com.lp.golfclub.model.ProductModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {

    private List<ProductModel> moviesList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        AppCompatTextView tvTitle;

        @BindView(R.id.tvProductTitle)
        AppCompatTextView tvProductTitle;

        @BindView(R.id.tvPrice)
        AppCompatTextView tvPrice;

        @BindView(R.id.rlMain)
        RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvTitle = view.findViewById(R.id.tvTitle);
            tvProductTitle = view.findViewById(R.id.tvProductTitle);
            tvPrice = view.findViewById(R.id.tvPrice);
        }
    }

    public ProductsAdapter(List<ProductModel> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_products, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProductModel productModel = moviesList.get(position);

        holder.tvTitle.setText(productModel.getTitle());
        holder.tvProductTitle.setText(productModel.getProductPlanTypeTitle());
        holder.tvPrice.setText("\u20B9 " +productModel.getNonMemberPrice() + " (incl. of taxes)");

        holder.rlMain.setOnClickListener(view ->
                ((MainActivity) activity).loadNewBooking(new BookingCalenderFragment(productModel)));
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
