package com.lp.golfclub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lp.golfclub.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorialAdapter extends RecyclerView.Adapter<TutorialAdapter.MyViewHolder> {

    private List<String> moviesList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivTutorial)
        AppCompatImageView ivTutorial;

        @BindView(R.id.tvTutorials)
        AppCompatTextView tvTutorials;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public TutorialAdapter(List<String> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tutorial, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (position == 0) {
            Glide.with(activity)
                    .load(R.drawable.tutorial_img_1)
                    .centerCrop()
                    .into(holder.ivTutorial);

            holder.tvTutorials.setText("Learning to Hit the Ball");
        } else {
            Glide.with(activity)
                    .load(R.drawable.tutorial_img_2)
                    .centerCrop()
                    .into(holder.ivTutorial);

            holder.tvTutorials.setText("Perfect your backswing");
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
