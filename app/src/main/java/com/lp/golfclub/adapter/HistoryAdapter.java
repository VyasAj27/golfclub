package com.lp.golfclub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.fragments.HistoryDetailFragment;
import com.lp.golfclub.model.BookingModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private List<BookingModel> alBookings;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rlMain)
        RelativeLayout rlMain;

        @BindView(R.id.tvOrderId)
        AppCompatTextView tvOrderId;

        @BindView(R.id.tvDate)
        AppCompatTextView tvDate;

        @BindView(R.id.tvTime)
        AppCompatTextView tvTime;

        @BindView(R.id.tvRupee)
        AppCompatTextView tvRupee;

        @BindView(R.id.tvOrderBy)
        AppCompatTextView tvOrderBy;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public HistoryAdapter(List<BookingModel> alBookings, Activity activity) {
        this.alBookings = alBookings;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        BookingModel bookingModel = alBookings.get(position);

        holder.tvOrderId.setText(bookingModel.getIncrementId());
        holder.tvDate.setText(bookingModel.getBookedToDate() + " \n" + bookingModel.getCoachingTiming());
        holder.tvRupee.setText("\u20B9"+bookingModel.getGuestTotal());
        holder.tvTime.setText(bookingModel.getCoachingTiming());
        holder.tvOrderBy.setText("Ordered by : "+bookingModel.getName());

        holder.rlMain.setOnClickListener(view -> ((MainActivity) activity).loadFragment(new HistoryDetailFragment(bookingModel)));
    }

    @Override
    public int getItemCount() {
        return alBookings.size();
    }
}
