package com.lp.golfclub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.fragments.GolfTerminologyFragment;
import com.lp.golfclub.interfaces.ClickCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlphabatesAdapter extends RecyclerView.Adapter<AlphabatesAdapter.MyViewHolder> {

    private List<String> alphabetsList;
    private Activity activity;
    private int selected;

    private ClickCallback clickCallback;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rlMain)
        RelativeLayout rlMain;

        @BindView(R.id.tvTime)
        AppCompatTextView tvTime;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            rlMain = view.findViewById(R.id.rlMain);
        }
    }

    public AlphabatesAdapter(List<String> alphabetsList, Activity activity, GolfTerminologyFragment clickCallback) {
        this.alphabetsList = alphabetsList;
        this.activity = activity;
        this.clickCallback = clickCallback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_alpha, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tvTime.setText(alphabetsList.get(position));
        if (position == selected) {
            holder.tvTime.setBackground(ContextCompat.getDrawable(activity, R.drawable.available_border));
        } else {
            holder.tvTime.setBackgroundColor(ContextCompat.getColor(activity, android.R.color.white));
        }
        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = position;
                clickCallback.onItemClick(holder.tvTime.getText().toString());
                notifyDataSetChanged();
            }
        });
    }


    @Override
    public int getItemCount() {
        return alphabetsList.size();
    }
}
