package com.lp.golfclub.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.model.TerminologyModel;
import com.lp.golfclub.util.CustomTypefaceSpan;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TerminologyAdapter extends RecyclerView.Adapter<TerminologyAdapter.MyViewHolder> implements Filterable {

    private List<TerminologyModel> alTerminologyAdapters;
    public List<TerminologyModel> alTerminologyAdaptersFiltered;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        AppCompatTextView tvTitle;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    alTerminologyAdaptersFiltered = alTerminologyAdapters;
                } else {
                    List<TerminologyModel> filteredList = new ArrayList<>();

                        for (TerminologyModel row : alTerminologyAdapters) {
                            if (row.getTitle().toLowerCase().contains(charSequence.toString().toLowerCase()) || row.getDesc().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                filteredList.add(row);
                            }
                        }

                    alTerminologyAdaptersFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = alTerminologyAdaptersFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                alTerminologyAdaptersFiltered = (ArrayList<TerminologyModel>) filterResults.values;

                // refresh the list with filtered data

                if (alTerminologyAdaptersFiltered.size() == 0) {
                    activity.runOnUiThread(() -> Toast.makeText(activity, "No result found.", Toast.LENGTH_SHORT).show());
                }

                notifyDataSetChanged();
            }
        };
    }


    public TerminologyAdapter(List<TerminologyModel> alTerminologyAdapters, Activity activity) {
        this.alTerminologyAdapters = alTerminologyAdapters;
        this.alTerminologyAdaptersFiltered = alTerminologyAdapters;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_terminology, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        TerminologyModel terminologyModel = alTerminologyAdaptersFiltered.get(position);

        Typeface typefaceRegular = ResourcesCompat.getFont(activity, R.font.montserrat_regular);
        Typeface typefaceBold = ResourcesCompat.getFont(activity, R.font.montserrat_bold);
        SpannableStringBuilder SS = new SpannableStringBuilder(terminologyModel.getTitle() + ": " + terminologyModel.getDesc());
        SS.setSpan(new CustomTypefaceSpan("", typefaceBold), 0, terminologyModel.getTitle().length() + 2, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", typefaceRegular), terminologyModel.getTitle().length() + 2, terminologyModel.getTitle().length() + 2 + terminologyModel.getDesc().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        holder.tvTitle.setText(SS);
    }

    @Override
    public int getItemCount() {
        return alTerminologyAdaptersFiltered.size();
    }
}
