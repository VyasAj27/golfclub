package com.lp.golfclub.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.lp.golfclub.R;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    @BindView(R.id.tvSecureLogin)
    AppCompatTextView tvSecureLogin;

    @BindView(R.id.tvForgetPass)
    AppCompatTextView tvForgetPass;

    @BindView(R.id.clSignUp)
    ConstraintLayout clSignUp;

    @BindView(R.id.tilPasssword)
    TextInputLayout tilPasssword;

    @BindView(R.id.etUserName)
    AppCompatEditText etUserName;

    @BindView(R.id.etPassword)
    AppCompatEditText etPassword;

    @BindView(R.id.ivFacebook)
    AppCompatImageView ivFacebook;

    @BindView(R.id.buttonFacebookLogin)
    LoginButton loginButton;

    @BindView(R.id.ivGoogle)
    AppCompatImageView ivGoogle;

    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    //    private FirebaseAuth mAuth;
    private boolean isFacebookLogin;
    private String fbToken = "";
    private String googleToken = "";

    private boolean isPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        tilPasssword.setTypeface(typefaceRegular);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut();

        initFacebookLogin();
        printKeyHash();
        initClickListener();
    }

    private void initFacebookLogin() {
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions("email");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.v("SignUpActivity", response.toString());

                            // Application code
                            try {
                                String name = object.getString("name");
                                String email = object.getString("email");
                                String id = object.getString("id");
                                fbToken = object.getString("id");
                                singleSignOn(name, email, "facebook", id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == AppConstants.REQUEST_GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                googleToken = account.getIdToken();
                singleSignOn(account.getDisplayName(), account.getEmail(), "google", account.getId());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }

        if (isFacebookLogin) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void singleSignOn(String name, String email, String service, String providerId) {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });


        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addParam(AppConstants.PARAM_FIRST_NAME, name);
        mHelper.addParam(AppConstants.PARAM_LAST_NAME, name);
        mHelper.addParam(AppConstants.PARAM_EMAIL, email);
        mHelper.addParam(AppConstants.PARAM_FB_TOKEN, fbToken);
        mHelper.addParam(AppConstants.PARAM_GOOGLE_TOKEN, googleToken);

        mHelper.post(AppConstants.URL_SIGN_UP, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    getUserDetails(mJsonObject.getString("user_id").trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void getUserDetails(String userId) {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_USER_ID, userId);

        mHelper.postFormData(AppConstants.URL_USER_DETAIL, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);
                    userData.setId(Integer.valueOf(mJsonObject.getString("user_id")));
                    userData.setName(mJsonObject.getString("name"));
                    userData.setEmail(mJsonObject.getString("email"));
                    userData.setMobile(mJsonObject.getString("mobile"));
                    userData.setGender(mJsonObject.getString("gender"));
                    userData.setProfileImage(mJsonObject.getString("photo"));
                    userData.setIdProof(mJsonObject.getString("photoid"));
//                    userData.da(mJsonObject.getString("dob"));
//                    userData.setGender(mJsonObject.getString("register_date"));
//                    userData.setGender(mJsonObject.getString("expiry_date"));
                    PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                    runOnUiThread(() -> {
                        PreferencesHelper.preferencesHelper.putBoolean(PreferencesHelper.PREF_LOGIN_SUCCESS, true);
                        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.lp.golfclub", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }


    private void initClickListener() {
        tvSecureLogin.setOnClickListener(this);
        clSignUp.setOnClickListener(this);
        tvForgetPass.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivGoogle.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSecureLogin:
                if (validate()) {
                    login();
                }
                break;
            case R.id.clSignUp:
                Intent registerIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(registerIntent);
                break;

            case R.id.tvForgetPass:
                Intent tvForgetPass = new Intent(LoginActivity.this, ForgotPassActivity.class);
                startActivity(tvForgetPass);
                break;
            case R.id.ivFacebook:
                isFacebookLogin = true;
                LoginManager.getInstance().logOut();
                loginButton.performClick();
                break;
            case R.id.ivGoogle:
                isFacebookLogin = false;
                signIn();
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, AppConstants.REQUEST_GOOGLE_SIGN_IN);
    }

    private void login() {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("username", etUserName.getText().toString());
        mHelper.addFormParam("password", etPassword.getText().toString());

        mHelper.postFormData(AppConstants.URL_LOGIN, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);

                    runOnUiThread(() -> {
                        if (webResponseModel.getMsg().equals("Not success!")) {
                            Toast.makeText(LoginActivity.this, webResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            PreferencesHelper.preferencesHelper.putBoolean(PreferencesHelper.PREF_LOGIN_SUCCESS, true);
                            PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mainIntent);
                            finish();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private boolean validate() {
        if (!AppValidator.isValidEmail(etUserName.getText())) {
            Toast.makeText(this, "Enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etPassword.getText())) {
            etPassword.setError("Enter password");
            etPassword.setFocusable(true);
            return false;
        }
        return true;
    }
}
