package com.lp.golfclub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.lp.golfclub.R;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "SignUpActivity";

    @BindView(R.id.tbHeader)
    Toolbar tbHeader;

    @BindView(R.id.tvSignIn)
    AppCompatTextView tvSignIn;

    @BindView(R.id.tvRegister)
    AppCompatTextView tvRegister;

    @BindView(R.id.tilPasssword)
    TextInputLayout tilPasssword;

    @BindView(R.id.tilConfirm)
    TextInputLayout tilConfirm;

    @BindView(R.id.etFirstName)
    AppCompatEditText etFirstName;

    @BindView(R.id.etLastName)
    AppCompatEditText etLastName;

    @BindView(R.id.etEmail)
    AppCompatEditText etEmail;

    @BindView(R.id.etAge)
    AppCompatEditText etAge;

    @BindView(R.id.tvGender)
    AppCompatTextView tvGender;

    @BindView(R.id.etMobile)
    AppCompatEditText etMobile;

    @BindView(R.id.etPassword)
    AppCompatEditText etPassword;

    @BindView(R.id.etConfirm)
    AppCompatEditText etConfirm;

    @BindView(R.id.etChoosePhoto)
    AppCompatEditText etChoosePhoto;

    @BindView(R.id.etAny)
    AppCompatEditText etAny;

    @BindView(R.id.tilAny)
    TextInputLayout tilAny;

    @BindView(R.id.tilChoosePhoto)
    TextInputLayout tilChoosePhoto;

    @BindView(R.id.rlGender)
    RelativeLayout rlGender;

    @BindView(R.id.ivFacebook)
    AppCompatImageView ivFacebook;

    @BindView(R.id.buttonFacebookLogin)
    LoginButton loginButton;

    @BindView(R.id.ivGoogle)
    AppCompatImageView ivGoogle;

    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    //    private FirebaseAuth mAuth;
    private boolean isFacebookLogin;
    private String fbToken = "";
    private String googleToken = "";
    private File selectedPhoto = null;
    private File selectedPhotoId = null;

    private boolean isPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        tvSignIn.setOnClickListener(this);
        tvRegister.setOnClickListener(this);

        etAny.setOnClickListener(v -> tilAny.performClick());
        etChoosePhoto.setOnClickListener(v -> tilChoosePhoto.performClick());

        ivFacebook.setOnClickListener(this);
        ivGoogle.setOnClickListener(this);

        tilChoosePhoto.setOnClickListener(v -> {
            isPhoto = true;
            EasyImage.openChooserWithGallery(SignUpActivity.this, "Capture Photo", 3);
        });
        tilAny.setOnClickListener(view -> {
            isPhoto = false;
            EasyImage.openChooserWithGallery(SignUpActivity.this, "Select ID Proof", 3);
        });

        tilPasssword.setTypeface(typefaceRegular);
        tilConfirm.setTypeface(typefaceRegular);

        rlGender.setOnClickListener(view -> {
            PopupMenu pum = new PopupMenu(SignUpActivity.this, rlGender);
            pum.inflate(R.menu.menu_gender);
            pum.setOnMenuItemClickListener(menuItem -> {
                tvGender.setText(menuItem.getTitle());
                return false;
            });
            pum.show();
        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut();

        initFacebookLogin();
        tbHeader.setTitle("");
        setSupportActionBar(tbHeader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initFacebookLogin() {
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions("email");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.v("SignUpActivity", response.toString());

                            // Application code
                            try {
                                String name = object.getString("name");
                                String email = object.getString("email");
                                String id = object.getString("id");
                                fbToken = object.getString("id");
                                singleSignOn(name, email, "facebook", id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, AppConstants.REQUEST_GOOGLE_SIGN_IN);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSignIn:
                finish();
                break;
            case R.id.tvRegister:
                if (validate()) {
                    register();
                }
                break;
            case R.id.ivFacebook:
                isFacebookLogin = true;
                loginButton.performClick();
                break;
            case R.id.ivGoogle:
                isFacebookLogin = false;
                signIn();
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                File file = list.get(0);
                try {
                    if (file != null) {
                        file = new Compressor(SignUpActivity.this).compressToFile(file);
                        if (isPhoto) {
                            selectedPhoto = file;
                            etChoosePhoto.setText(file.getName());
                        } else {
                            selectedPhotoId = file;
                            etAny.setText(file.getName());
                        }
//                        uploadMedia(selectedPhoto);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == AppConstants.REQUEST_GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                googleToken = account.getIdToken();
                singleSignOn(account.getDisplayName(), account.getEmail(), "google", account.getId());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }

        if (isFacebookLogin) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void singleSignOn(String name, String email, String service, String providerId) {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });


        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addParam(AppConstants.PARAM_FIRST_NAME, name);
        mHelper.addParam(AppConstants.PARAM_LAST_NAME, name);
        mHelper.addParam(AppConstants.PARAM_EMAIL, email);

        if(providerId.equals("google")){
            mHelper.addParam(AppConstants.PARAM_GOOGLE_TOKEN, googleToken);
        }
       else{
            mHelper.addParam(AppConstants.PARAM_FB_TOKEN, fbToken);
        }

        mHelper.post(AppConstants.URL_SIGN_UP, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    getUserDetails(mJsonObject.getString("user_id").trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(SignUpActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

/*
    private String generateBase64(File imgFile) {
        if (imgFile.exists() && imgFile.length() > 0) {
            Bitmap bm = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, bOut);
            return Base64.encodeToString(bOut.toByteArray(), Base64.NO_WRAP);
        }
        return null;
    }
*/

    private boolean validate() {
        if (!AppValidator.isEmpty(etFirstName.getText())) {
            Toast.makeText(this, "Enter valid first name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etLastName.getText())) {
            Toast.makeText(this, "Enter valid last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isValidEmail(etEmail.getText())) {
            Toast.makeText(this, "Enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (tvGender.getText().toString().equals("Gender")) {
            Toast.makeText(this, "Select Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etChoosePhoto.getText())) {
            Toast.makeText(this, "Select profile picture", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etAny.getText())) {
            Toast.makeText(this, "Select Any ID", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etMobile.getText())) {
            Toast.makeText(this, "Enter mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etMobile.getText().toString().length() != 10) {
            Toast.makeText(this, "Mobile number should be of 10 digit.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etPassword.getText())) {
            Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etConfirm.getText())) {
            Toast.makeText(this, "Enter confirm password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!etPassword.getText().toString().equals(etConfirm.getText().toString())) {
            Toast.makeText(this, "Password not matching check your passwords.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void register() {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_FIRST_NAME, etFirstName.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_LAST_NAME, etLastName.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_PASSWORD, etPassword.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_EMAIL, etEmail.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_MOBILE, etMobile.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_AGE, etAge.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_GENDER, tvGender.getText().toString().trim());
        mHelper.addFile(AppConstants.PARAM_PROFILE_IMAGE, selectedPhoto.getName(), selectedPhoto);
        mHelper.addFile(AppConstants.PARAM_ID_PROOF, selectedPhotoId.getName(), selectedPhotoId);
        mHelper.addFormParam(AppConstants.PARAM_FB_TOKEN, fbToken);
        mHelper.addFormParam(AppConstants.PARAM_GOOGLE_TOKEN, googleToken);

        mHelper.postFormData(AppConstants.URL_SIGN_UP, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    if(TextUtils.isEmpty(webResponseModel.getData())){
                        runOnUiThread(() -> {
                            Toast.makeText(SignUpActivity.this, webResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                        });
                    }
                    else{
                        UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);

                        runOnUiThread(() -> {
                            if(TextUtils.isEmpty(userData.getEmail())){
                                Toast.makeText(SignUpActivity.this, webResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                            else{
                                PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                                Intent mainIntent = new Intent(SignUpActivity.this, OtpVerificationActivity.class);
//                        mainIntent.putExtra("OTP", otp);
                                startActivity(mainIntent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(SignUpActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void getUserDetails(String userId) {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_USER_ID, userId);

        mHelper.postFormData(AppConstants.URL_USER_DETAIL, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);
                    userData.setId(Integer.valueOf(mJsonObject.getString("user_id")));
                    userData.setName(mJsonObject.getString("name"));
                    userData.setEmail(mJsonObject.getString("email"));
                    userData.setMobile(mJsonObject.getString("mobile"));
                    userData.setGender(mJsonObject.getString("gender"));
                    userData.setProfileImage(mJsonObject.getString("photo"));
                    userData.setIdProof(mJsonObject.getString("photoid"));
//                    userData.da(mJsonObject.getString("dob"));
//                    userData.setGender(mJsonObject.getString("register_date"));
//                    userData.setGender(mJsonObject.getString("expiry_date"));
                    PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                    runOnUiThread(() -> {
                        Intent mainIntent = new Intent(SignUpActivity.this, MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(SignUpActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

}
