package com.lp.golfclub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.facebook.FacebookSdk;
import com.lp.golfclub.R;
import com.lp.golfclub.util.PreferencesHelper;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        PreferencesHelper.getInstance(getApplicationContext());


        FacebookSdk.sdkInitialize(getApplicationContext());

        new Handler().postDelayed(() -> {

           if(PreferencesHelper.preferencesHelper != null && PreferencesHelper.preferencesHelper.contains(PreferencesHelper.PREF_LOGIN_SUCCESS)) {
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            } else {
                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, 2000);
    }

}
