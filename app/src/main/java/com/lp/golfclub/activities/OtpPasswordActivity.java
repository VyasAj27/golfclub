package com.lp.golfclub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.lp.golfclub.R;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtpPasswordActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tbHeader)
    Toolbar tbHeader;

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.tilNewPass)
    TextInputLayout tilNewPass;

    @BindView(R.id.tilConfirmPass)
    TextInputLayout tilConfirmPass;

    @BindView(R.id.etOtp)
    AppCompatEditText etOtp;

    @BindView(R.id.etNewPass)
    AppCompatEditText etNewPass;

    @BindView(R.id.etConfirmPass)
    AppCompatEditText etConfirmPass;

    private String otp , email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_password);
        ButterKnife.bind(this);

        otp = getIntent().getStringExtra("OTP");
        email = getIntent().getStringExtra("email");

        tvSubmit.setOnClickListener(this);

        tilConfirmPass.setTypeface(typefaceRegular);
        tilNewPass.setTypeface(typefaceRegular);

        tbHeader.setTitle("");
        setSupportActionBar(tbHeader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSubmit:
                if (!AppValidator.isEmpty(etOtp.getText().toString())) {
                    Toast.makeText(this, "Enter valid OTP", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!AppValidator.isEmpty(etNewPass.getText())) {
                    Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!AppValidator.isEmpty(etConfirmPass.getText())) {
                    Toast.makeText(this, "Enter confirm password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!etNewPass.getText().toString().equals(etConfirmPass.getText().toString())) {
                    Toast.makeText(this, "Password not matching check your passwords.", Toast.LENGTH_SHORT).show();
                    return;
                }
                validateOTP();
                break;
        }
    }
    private void validateOTP() {
        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_EMAIL, email);
        mHelper.addFormParam("new_password", etNewPass.getText().toString());
        mHelper.addFormParam("otp", etOtp.getText().toString());

        mHelper.postFormData(AppConstants.URL_validateOTP, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    getUserDetails(mJsonObject.getString("user_id").trim());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(OtpPasswordActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void getUserDetails(String userId) {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_USER_ID, userId);

        mHelper.postFormData(AppConstants.URL_USER_DETAIL, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);
                    userData.setId(Integer.valueOf(mJsonObject.getString("user_id")));
                    userData.setName(mJsonObject.getString("name"));
                    userData.setEmail(mJsonObject.getString("email"));
                    userData.setMobile(mJsonObject.getString("mobile"));
                    userData.setGender(mJsonObject.getString("gender"));
                    userData.setProfileImage(mJsonObject.getString("photo"));
                    userData.setIdProof(mJsonObject.getString("photoid"));
//                    userData.da(mJsonObject.getString("dob"));
//                    userData.setGender(mJsonObject.getString("register_date"));
//                    userData.setGender(mJsonObject.getString("expiry_date"));
                    PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                    runOnUiThread(() -> {
                        PreferencesHelper.preferencesHelper.putBoolean(PreferencesHelper.PREF_LOGIN_SUCCESS, true);
                        Intent mainIntent = new Intent(OtpPasswordActivity.this, MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(OtpPasswordActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }
}
