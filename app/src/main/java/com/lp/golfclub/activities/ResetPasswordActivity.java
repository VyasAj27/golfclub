package com.lp.golfclub.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.lp.golfclub.R;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetPasswordActivity extends BaseActivity implements View.OnClickListener {


    @BindView(R.id.tbHeader)
    Toolbar tbHeader;

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.tilOldPass)
    TextInputLayout tilOldPass;

    @BindView(R.id.tilNewPass)
    TextInputLayout tilNewPass;

    @BindView(R.id.tilConfirmPass)
    TextInputLayout tilConfirmPass;

    @BindView(R.id.etOldPass)
    AppCompatEditText etOldPass;

    @BindView(R.id.etNewPass)
    AppCompatEditText etNewPass;

    @BindView(R.id.etConfirmPass)
    AppCompatEditText etConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        tvSubmit.setOnClickListener(this);

        tilOldPass.setTypeface(typefaceRegular);
        tilConfirmPass.setTypeface(typefaceRegular);
        tilNewPass.setTypeface(typefaceRegular);

        tbHeader.setTitle("");
        setSupportActionBar(tbHeader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSubmit:
                if (!AppValidator.isEmpty(etOldPass.getText().toString())) {
                    Toast.makeText(this, "Enter old password", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!AppValidator.isEmpty(etNewPass.getText())) {
                    Toast.makeText(this, "Enter new password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!AppValidator.isEmpty(etConfirmPass.getText())) {
                    Toast.makeText(this, "Enter confirm password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!etNewPass.getText().toString().equals(etConfirmPass.getText().toString())) {
                    Toast.makeText(this, "New password not matching check your passwords.", Toast.LENGTH_SHORT).show();
                    return;
                }

                resetPassword();

                break;
        }
    }

    private void resetPassword() {

        UserData userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("user_id", String.valueOf(userData.getId()));
        mHelper.addFormParam("new_password", etNewPass.getText().toString());
        mHelper.addFormParam("old_password", etOldPass.getText().toString());

        mHelper.postFormData(AppConstants.URL_RESET, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);

                    runOnUiThread(() -> {
                        if(webResponseModel.getMsg().equals("Incorrect old password!")){
                            Toast.makeText(ResetPasswordActivity.this, "" + webResponseModel.getMsg(), Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(ResetPasswordActivity.this, "" + webResponseModel.getMsg(), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(ResetPasswordActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }
}
