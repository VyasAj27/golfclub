package com.lp.golfclub.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.lp.golfclub.R;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.fragments.ContactUsFragment;
import com.lp.golfclub.fragments.GolfTerminologyFragment;
import com.lp.golfclub.fragments.HistoryOfGolfFragment;
import com.lp.golfclub.fragments.MainFragment;
import com.lp.golfclub.fragments.MyProfileFragment;
import com.lp.golfclub.fragments.NewBookingFragment;
import com.lp.golfclub.fragments.NotificationFragment;
import com.lp.golfclub.fragments.ProductListFragment;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.PreferencesHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.clPrimaryToolbar)
    ConstraintLayout clPrimaryToolbar;

    @BindView(R.id.clSecondaryToolbar)
    ConstraintLayout clSecondaryToolbar;

    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.ivDrawer)
    AppCompatImageView ivDrawer;

    @BindView(R.id.ivSecDrawer)
    AppCompatImageView ivSecDrawer;

    @BindView(R.id.ivClose)
    AppCompatImageView ivClose;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.tvHome)
    AppCompatTextView tvHome;

    @BindView(R.id.tvMyProfile)
    AppCompatTextView tvMyProfile;

    @BindView(R.id.tvSlotBooking)
    AppCompatTextView tvSlotBooking;

    @BindView(R.id.tvHistory)
    AppCompatTextView tvHistory;

    @BindView(R.id.tvTerminology)
    AppCompatTextView tvTerminology;

    @BindView(R.id.tvTutorials)
    AppCompatTextView tvTutorials;

    @BindView(R.id.tvNotifications)
    AppCompatTextView tvNotifications;

    @BindView(R.id.tvContact)
    AppCompatTextView tvContact;

    @BindView(R.id.tvLogoutApp)
    AppCompatTextView tvLogoutApp;


    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ivBack.setOnClickListener(this);
        ivDrawer.setOnClickListener(this);
        ivSecDrawer.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        tvSlotBooking.setOnClickListener(this);
        tvHome.setOnClickListener(this);
        tvMyProfile.setOnClickListener(this);
        tvHistory.setOnClickListener(this);
        tvTerminology.setOnClickListener(this);
        tvTutorials.setOnClickListener(this);
        tvContact.setOnClickListener(this);
        tvNotifications.setOnClickListener(this);
        tvLogoutApp.setOnClickListener(this);

        manageDrawer(tvHome);
        loadFragment(new MainFragment());

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        sendRegistrationToServer(token);
                    }
                });
    }

    private void sendRegistrationToServer(String token) {

        UserData userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();

        mHelper.addFormParam("user_id", userData.getId().toString());
        mHelper.addParam("fcm_token", token);

        mHelper.postFormData(AppConstants.URL_UPDATE_TOKEN, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {
            }

            @Override
            public void onError(final String error) {
            }
        }, WebServiceHelper.METHOD_POST);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment topFragment = getTopFragment();
        if (topFragment == null) {
            finish();
        } else {
            currentFragment = topFragment;
            setCurrentSelected();
        }
    }

    public void showPrimaryToolbar() {
        clPrimaryToolbar.setVisibility(View.VISIBLE);
        clSecondaryToolbar.setVisibility(View.GONE);
    }

    public void showSecondaryToolbar(String title) {
        tvTitle.setText(title);
        clPrimaryToolbar.setVisibility(View.GONE);
        clSecondaryToolbar.setVisibility(View.VISIBLE);
    }

    public void loadFragment(Fragment fragment) {
        replaceFragment(fragment, R.id.frameContainer);
    }

    public void loadNewBooking(Fragment fragment) {
        manageDrawer(tvSlotBooking);
        replaceFragment(fragment, R.id.frameContainer);
    }

    public void loadTerminology(Fragment fragment) {
        manageDrawer(tvTerminology);
        replaceFragment(fragment, R.id.frameContainer);
    }

    public void loadBookingHistory(Fragment fragment) {
        replaceFragment(fragment, R.id.frameContainer);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.ivDrawer:
                drawerLayout.openDrawer(GravityCompat.END);
                break;
            case R.id.ivSecDrawer:
                drawerLayout.openDrawer(GravityCompat.END);
                break;
            case R.id.ivClose:
                drawerLayout.closeDrawer(GravityCompat.END);
                break;
            case R.id.tvHome:
                if (!tvHome.isSelected()) {
                    loadFragment(new MainFragment());
                    manageDrawer(tvHome);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.tvMyProfile:
                if (!tvMyProfile.isSelected()) {
                    loadFragment(new MyProfileFragment());
                    manageDrawer(tvMyProfile);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.tvSlotBooking:
                if (!tvSlotBooking.isSelected()) {
                    loadFragment(new ProductListFragment());
                    manageDrawer(tvSlotBooking);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.tvHistory:
                if (!tvHistory.isSelected()) {
                    loadFragment(new HistoryOfGolfFragment());
                    manageDrawer(tvHistory);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.tvTerminology:
                if (!tvTerminology.isSelected()) {
                    loadFragment(new GolfTerminologyFragment());
                    manageDrawer(tvTerminology);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.tvTutorials:
                drawerLayout.closeDrawer(GravityCompat.END);
                break;
            case R.id.tvContact:
                if (!tvContact.isSelected()) {
                    loadFragment(new ContactUsFragment());
                    manageDrawer(tvContact);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.tvLogoutApp:
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PreferencesHelper.preferencesHelper.clear();
                                Intent mainIntent = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(mainIntent);
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                drawerLayout.closeDrawer(GravityCompat.END);

                break;
            case R.id.tvNotifications:
                if (!tvNotifications.isSelected()) {
                    loadFragment(new NotificationFragment());
                    manageDrawer(tvNotifications);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setCurrentSelected() {
        if (currentFragment instanceof MainFragment) {
            manageDrawer(tvHome);
        }
        if (currentFragment instanceof NewBookingFragment) {
            manageDrawer(tvSlotBooking);
        }
        if (currentFragment instanceof MyProfileFragment) {
            manageDrawer(tvMyProfile);
        }
        if (currentFragment instanceof ContactUsFragment) {
            manageDrawer(tvContact);
        }
    }

    private void resetAllTextViews() {
        Typeface face = ResourcesCompat.getFont(this, R.font.montserrat_regular);
        tvHome.setTypeface(face);
        tvMyProfile.setTypeface(face);
        tvSlotBooking.setTypeface(face);
        tvHistory.setTypeface(face);
        tvTerminology.setTypeface(face);
        tvTutorials.setTypeface(face);
        tvNotifications.setTypeface(face);
        tvContact.setTypeface(face);

        tvHome.setSelected(false);
        tvMyProfile.setSelected(false);
        tvSlotBooking.setSelected(false);
        tvHistory.setSelected(false);
        tvTerminology.setSelected(false);
        tvTutorials.setSelected(false);
        tvNotifications.setSelected(false);
        tvContact.setSelected(false);
    }

    private void manageDrawer(AppCompatTextView textView) {
        resetAllTextViews();
        Typeface face = ResourcesCompat.getFont(this, R.font.montserrat_bold);
        textView.setSelected(true);
        textView.setTypeface(face);
        drawerLayout.closeDrawer(GravityCompat.END);
        hideKeyboard(this);
    }
}
