package com.lp.golfclub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.lp.golfclub.R;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgotPassActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tbHeader)
    Toolbar tbHeader;

    @BindView(R.id.tvSignIn)
    AppCompatTextView tvSignIn;

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.etEmail)
    AppCompatEditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);

        tvSignIn.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);

        tbHeader.setTitle("");
        setSupportActionBar(tbHeader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSignIn:
                finish();
                break;
            case R.id.tvSubmit:
                if (!AppValidator.isValidEmail(etEmail.getText().toString())) {
                    Toast.makeText(this, "Enter valid email", Toast.LENGTH_LONG).show();
                    return;
                }
                forgot();
                break;
        }
    }

    private void forgot() {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("email", etEmail.getText().toString());

        mHelper.postFormData(AppConstants.URL_FORGOT, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);

                    runOnUiThread(() -> {
                        if(TextUtils.isEmpty(webResponseModel.getData())){
                            Toast.makeText(ForgotPassActivity.this, webResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Intent mainIntent = new Intent(ForgotPassActivity.this, OtpPasswordActivity.class);
                            try {
                                JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                                mainIntent.putExtra("OTP", mJsonObject.getString("otp"));
                                mainIntent.putExtra("email",etEmail.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            startActivity(mainIntent);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(ForgotPassActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }
}
