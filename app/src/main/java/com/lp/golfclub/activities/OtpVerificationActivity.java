package com.lp.golfclub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.lp.golfclub.R;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tbHeader)
    Toolbar tbHeader;

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.tvResendApp)
    AppCompatTextView tvResendApp;

    @BindView(R.id.etOtp)
    AppCompatEditText etOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        ButterKnife.bind(this);
        tvSubmit.setOnClickListener(this);
        tvResendApp.setOnClickListener(this);

//        otp = getIntent().getStringExtra("OTP");

        tbHeader.setTitle("");
        setSupportActionBar(tbHeader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvResendApp:
//                sendOTP();
                break;
            case R.id.tvSubmit:
                if (!AppValidator.isEmpty(etOtp.getText().toString())) {
                    Toast.makeText(this, "Enter valid OTP", Toast.LENGTH_LONG).show();
                    return;
                }
                validateRegistrationOTP();
                break;
        }

    }

    private void validateRegistrationOTP() {

        UserData userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_EMAIL, userData.getEmail());
        mHelper.addFormParam(AppConstants.PARAM_MOBILE, userData.getMobile());
        mHelper.addFormParam("otp", etOtp.getText().toString());

        mHelper.postFormData(AppConstants.URL_validateRegistrationOTP, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    getUserDetails(mJsonObject.getString("user_id").trim());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(OtpVerificationActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void getUserDetails(String userId) {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_USER_ID, userId);

        mHelper.postFormData(AppConstants.URL_USER_DETAIL, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);
                    userData.setId(Integer.valueOf(mJsonObject.getString("user_id")));
                    userData.setName(mJsonObject.getString("name"));
                    userData.setEmail(mJsonObject.getString("email"));
                    userData.setMobile(mJsonObject.getString("mobile"));
                    userData.setGender(mJsonObject.getString("gender"));
                    userData.setProfileImage(mJsonObject.getString("photo"));
                    userData.setIdProof(mJsonObject.getString("photoid"));
//                    userData.da(mJsonObject.getString("dob"));
//                    userData.setGender(mJsonObject.getString("register_date"));
//                    userData.setGender(mJsonObject.getString("expiry_date"));
                    PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                    runOnUiThread(() -> {
                        PreferencesHelper.preferencesHelper.putBoolean(PreferencesHelper.PREF_LOGIN_SUCCESS, true);
                        Intent mainIntent = new Intent(OtpVerificationActivity.this, MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(OtpVerificationActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

   /* private void sendOTP() {

        runOnUiThread(() -> {
            Utils.showProgressbar(this);
        });
        UserData userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        final WebServiceHelper mHelper = WebServiceHelper.getInstance(this);

        mHelper.post(AppConstants.URL_SEND_OTP + userData.getMobile().toString().trim(), new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);

                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());

                    otp = mJsonObject.getString("otp");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {
                runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(OtpVerificationActivity.this, ""+error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_GET);
    }*/
}
