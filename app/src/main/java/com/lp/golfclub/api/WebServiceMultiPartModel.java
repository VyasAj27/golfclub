package com.lp.golfclub.api;

import java.io.File;


/**
 * model class for Web Service MultiPart
 *
 * @author Atmiya
 * @since 26-09-2018.
 */

public class WebServiceMultiPartModel {

    private String image_name;
    private File image_file;

    public WebServiceMultiPartModel(String image_name, File image_file) {
        this.image_name = image_name;
        this.image_file = image_file;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public File getImage_file() {
        return image_file;
    }

    public void setImage_file(File image_file) {
        this.image_file = image_file;
    }
}