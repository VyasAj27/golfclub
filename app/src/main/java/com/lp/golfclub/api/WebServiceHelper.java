package com.lp.golfclub.api;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;

import com.google.gson.Gson;
import com.lp.golfclub.MainApplication;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WebServiceHelper {

    private static final Object lockObject = new Object();
    private static WebServiceHelper webServiceHelper;
    private final Handler handler = new Handler(Looper.getMainLooper());

    private static ArrayMap<String, String> headers = new ArrayMap<>();
    private static ArrayMap<String, String> params;
    private static ArrayMap<String, WebServiceMultiPartModel> fileParams;

    public static final int METHOD_POST = 0;
    public static final int METHOD_PUT = 1; // Make it public when needed
    public static final int METHOD_GET = 2;
    private static final int METHOD_DELETE = 3; // Make it public when needed

    //For passing jason object in request
    public static JSONObject requestJsonObject;
    private MediaType JSON = MediaType.parse("application/json");

    public static boolean isRunning = false;
    private static boolean isCancelling = false;
    private OkHttpClient client;

    /**
     * Interface to handle Callbacks
     */
    public interface WebServiceListener {
        void onSuccess(String response);

        void onError(String error);
    }

    public void cancellAll() {
        isCancelling = true;
        client.dispatcher().cancelAll();
    }

    public static WebResponseModel getResponseModel(String response) throws JSONException {
        WebResponseModel webModel = new WebResponseModel();
        String success = "";
        String data = "";
        String message = "";

        Logger.w("Data=\n" + response);

        if (!response.isEmpty()) {

            //Parse result
            JSONObject mJsonObject = new JSONObject(response);

            if (mJsonObject.has("data") && !mJsonObject.isNull("data")) {
                data = getJsonStringValue("data", mJsonObject);
            }
            if (mJsonObject.has("message") && !mJsonObject.isNull("message")) {
                message = getJsonStringValue("message", mJsonObject);
            }
        }

        webModel.setSuccess(success);
        webModel.setData(data);
        webModel.setMsg(message);

        return webModel;
    }

    /**
     * Default Constructor
     */
    private WebServiceHelper() {

    }

    /**
     * Method to return Singleton object
     *
     * @return WebServiceHelper object
     */
    public static WebServiceHelper getInstance() {
        if (webServiceHelper == null)
            synchronized (lockObject) {
                if (webServiceHelper == null)
                    webServiceHelper = new WebServiceHelper();
            }

        params = new ArrayMap<>();
        requestJsonObject = new JSONObject();
        fileParams = new ArrayMap<>();
        headers.clear();

        return webServiceHelper;
    }

    /**
     * Add parameter
     *
     * @param key
     * @param value
     */
    public void addFormParam(String key, String value) {
        params.put(key, value);
    }

    /**
     * Method to pass File Parameter
     *
     * @param key      Param Key
     * @param fileName File name
     * @param file     File
     */
    public void addFile(String key, String fileName, File file) {
        WebServiceMultiPartModel multiPartFile = new WebServiceMultiPartModel(fileName, file);
        fileParams.put(key, multiPartFile);
    }

    /**
     * Add Header
     *
     * @param key
     * @param value
     */
    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    /**
     * Add Headers to Request.Builder
     *
     * @param builder object of request builder
     */
    private void addHeaders(Request.Builder builder) {
        for (String key : headers.keySet()) {
            Logger.w("Header = " + key + " : " + headers.get(key));
            builder.addHeader(key, headers.get(key));
        }
    }

    /**
     * Add json parameter
     *
     * @param key
     * @param value
     */
    public void addParam(String key, String value) {
        try {
            requestJsonObject.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.e("JsonEception" + e.getMessage());
        }
    }

    /**
     * Add json parameter
     *
     * @param key
     * @param value
     */
    public void addParam(String key, int value) {
        try {
            requestJsonObject.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.e("JsonEception" + e.getMessage());
        }
    }

    /**
     * Add json parameter
     *
     * @param key
     * @param value
     */
    public void addParam(String key, double value) {
        try {
            requestJsonObject.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.e("JsonEception" + e.getMessage());
        }
    }

    /**
     * Add json parameter
     *
     * @param key
     * @param value
     */
    public void addArrayParam(String key, String value) {
        try {
            requestJsonObject.put(key, new JSONArray(value));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Add json parameter
     *
     * @param key
     * @param value
     */
    public void addObjParam(String key, String value) {
        try {
            requestJsonObject.put(key, new JSONObject(value));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to prepare headers, params, url and Execute POST request
     *
     * @param serviceUrl         Webservice url
     * @param restClientListener RestClientListener object
     * @param requestType        type of request i.e. POST,PUT
     */
    public void post(@NonNull String serviceUrl, @NonNull final WebServiceListener restClientListener, int requestType) {

        isRunning = true;

        final Request.Builder builder = new Request.Builder();
        Log.v("TAG", "URL = " + serviceUrl);
        builder.addHeader("Content-Type", "application/json");
        builder.url(serviceUrl);
        if (requestJsonObject.length() > 0) {
            if (requestType == METHOD_POST)
                builder.post(generateRequestBody());
            else if (requestType == METHOD_PUT)
                builder.put(generateRequestBody());
            else if (requestType == METHOD_DELETE)
                builder.delete(generateRequestBody());
        } else {
            if (requestType == METHOD_DELETE)
                builder.delete();
            else if (requestType == METHOD_GET)
                builder.get();
            else if (requestType == METHOD_PUT)
                builder.put(generateEmptyRequestBody());
        }

        execute(builder, restClientListener);
    }

    public void postFormData(@NonNull String serviceUrl, @NonNull final WebServiceListener restClientListener, int requestType) {

        isRunning = true;

        final Request.Builder builder = new Request.Builder();
        Log.v("TAG", "URL = " + serviceUrl);
        builder.url(serviceUrl);
        builder.addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
        builder.post(generateFormRequestBody());
        execute(builder, restClientListener);
    }


    /**
     * Executes request and post response to RestClientListener
     *
     * @param builder
     * @param restClientListener
     */
    private void execute(final Request.Builder builder, final WebServiceListener restClientListener) {


        final OkHttpClient.Builder builderNew = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.MINUTES);
        client = builderNew.build();

        try {

            client.newCall(builder.build()).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, final IOException e) {
                    isRunning = false;

                    e.printStackTrace();
                    handler.post(() -> {

                        /*
                           This condition is here for, when internet is not connected and cached data is not available
                         */
                        try {
                            final String resp = getResponse(builder);
                            if (!Utils.isNetworkConnected(MainApplication.mContext) && resp != null) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            restClientListener.onSuccess(resp);
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                });
                            } else {
                                if (Utils.isNetworkConnected(MainApplication.mContext)) {
                                    if (isCancelling) {
                                        isCancelling = false;
                                    }
                                    if (resp != null)
                                        restClientListener.onError("Error receiving data");
                                    else
                                        restClientListener.onError("Server not responding");
                                } else {
                                    restClientListener.onError("No Internet connection");
                                }
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                        restClientListener.onError("on Failure error ");
                                }
                            });
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    isRunning = false;

                    Log.v("TAG", "Data=" + response.toString());

                    try {

                        if (!response.isSuccessful()) {

                            Log.v("TAG", "response code=" + response.code());
                            Log.v("TAG", "response fail=" + response.networkResponse());

                            if (response.code() == 422) {
                                WebResponseModel webResponseModel = getResponseModel(response.body().string());
                                restClientListener.onError(webResponseModel.getData());
                            } else {
                                restClientListener.onError("Unable to process request. Check details.");
                            }
                            return;
                        }

                        restClientListener.onSuccess(response.body().string());

                    } catch (Exception e) {
                        e.printStackTrace();
                        restClientListener.onError(String.valueOf(response.code()));
                    }
                }
            });

        } catch (Exception e) {
            isRunning = false;
            e.printStackTrace();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    restClientListener.onError("Error! 2");
                }
            });
        }
    }

    /**
     * Create RequestBody from params
     *
     * @return RequestBody object
     */
    private RequestBody generateFormRequestBody() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        for (String key : params.keySet()) {
            Log.v("TAG", "params = " + key + " : " + params.get(key));
            builder.addFormDataPart(key, params.get(key));
        }
        if (fileParams != null)
            for (String key : fileParams.keySet()) {
                Log.v("TAG", "file params = " + key + " : " + fileParams.get(key));
                builder.addFormDataPart(key, fileParams.get(key).getImage_name(),
                        RequestBody.create(MediaType.parse(getMimeType(fileParams.get(key).getImage_file())), fileParams.get(key).getImage_file()));
            }
        return builder.build();
    }

    /**
     * Create JSON RequestBody from params
     *
     * @return RequestBody object
     */
    private RequestBody generateRequestBody() {
        Logger.w("Request Body = " + requestJsonObject.toString());
        return RequestBody.create(JSON, requestJsonObject.toString());
    }

    /**
     * Returns empty RequestBody for requests with no parameters
     *
     * @return RequestBody object
     */
    private RequestBody generateEmptyRequestBody() {
        return new FormBody.Builder().build();
    }


    /**
     * Save PrinterData to preference using Request.Builder as key
     *
     * @param request  key
     * @param response value
     */
    public void saveResponse(Request.Builder request, WebResponseModel response) {
//        PrefsHelper.putValue(new Gson().toJson(request), new Gson().toJson(response));
    }

    /**
     * Save PrinterData to preference using Request.Builder as key
     */
    public void clearRunningCalls() {
        //go through the running calls and
        for (Call call : client.dispatcher().runningCalls()) {
            call.cancel();
        }

    }

    /**
     * Retrieves WebResponseModel from preference using Request.Builder as key
     *
     * @param request key
     * @return value
     */
    public String getResponse(Request.Builder request) {
        try {
            String resp = PreferencesHelper.getValue(new Gson().toJson(request));
            Logger.w("resp cache=" + resp);
            return resp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get Boolean Value from JSONObject
     *
     * @param tag         string tag to be parsed
     * @param mJsonObject source JSONObject
     * @return boolean value
     * @throws JSONException
     */
    public static boolean getJsonBoolValue(String tag, JSONObject mJsonObject) throws JSONException {
        if (mJsonObject.has(tag))
            return mJsonObject.getBoolean(tag);
        return false;
    }


    /**
     * Get String Value from JSONObject
     *
     * @param tag         string tag to be parsed
     * @param mJsonObject source JSONObject
     * @return string value
     * @throws JSONException
     */
    public static String getJsonStringValue(String tag, JSONObject mJsonObject) throws JSONException {
        if (mJsonObject.has(tag))
            return mJsonObject.getString(tag);
        return "";
    }

    /**
     * Method to get the File type
     *
     * @param mFile File
     * @return String File Type
     */
    private String getMimeType(File mFile) {
        String type = "*/*";
        try {
            String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(mFile).toString());
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.w("type===" + type);
        return type;
    }

    /*private RequestBody generateRequestBody() {
        final JSONObject jsonObj = new JSONObject();
        if (params != null && params.size() > 0) {
            for (String key : params.keySet()) {
                try {
                    jsonObj.put(key, params.get(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        Logger.w(String.valueOf(jsonObj));

// Logger.d("Params = \n" + String.valueOf(jsonObj));
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), String.valueOf(jsonObj));
        return requestBody;
    }*/
}