package com.lp.golfclub.api;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.lp.golfclub.MainApplication;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;
import com.orhanobut.logger.Logger;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;


public class MultipartRequest {
    private static final Object lockObject = new Object();
    private final Handler handler = new Handler(Looper.getMainLooper());
    private static MultipartRequest webServiceHelper;
    public static MultipartBuilder builder;
    private static WeakReference<Activity> mActivity;
    private static OkHttpClient client;

    public static MultipartRequest getInstance(Activity mAct) {
        if (webServiceHelper == null)
            synchronized (lockObject) {
                if (webServiceHelper == null)
                    webServiceHelper = new MultipartRequest();
            }
        mActivity = new WeakReference<>(mAct);
        builder = new MultipartBuilder();

        builder.type(MultipartBuilder.FORM);
        client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        return webServiceHelper;
    }

    static class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Log.d("OkHttp", "request " + request.url() + " on " + request.body());

            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Log.d("OkHttp", String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            return response;
        }
    }


    public static WebResponseModel getResponseModel(String response) throws JSONException {
        WebResponseModel webModel = new WebResponseModel();
        String success = "";
        String data = "";
        String message = "";

        Logger.w("Data=\n" + response);

        if (!response.isEmpty()) {

            //Parse result
            JSONObject mJsonObject = new JSONObject(response);

            if (mJsonObject.has("success") && !mJsonObject.isNull("success"))
                success = getJsonStringValue("success", mJsonObject);

            if (mJsonObject.has("data") && !mJsonObject.isNull("data")) {
                data = getJsonStringValue("data", mJsonObject);
            }
            if (mJsonObject.has("msg") && !mJsonObject.isNull("msg")) {
                message = getJsonStringValue("msg", mJsonObject);
            }
        }

        webModel.setSuccess(success);
        webModel.setData(data);
        webModel.setMsg(message);

        return webModel;
    }

    /**
     * Get String Value from JSONObject
     *
     * @param tag         string tag to be parsed
     * @param mJsonObject source JSONObject
     * @return string value
     * @throws JSONException
     */
    public static String getJsonStringValue(String tag, JSONObject mJsonObject) throws JSONException {
        if (mJsonObject.has(tag))
            return mJsonObject.getString(tag);
        return "";
    }


    /**
     * Interface to handle Callbacks
     */
    public interface WebServiceListener {
        void onSuccess(String response);

        void onError(String error);
    }

    public void addParam(String name, String value) {
        this.builder.addFormDataPart(name, value);
    }

    public void addFile(String name, File file) {
        this.builder.addFormDataPart(name, file.getName(), RequestBody.create(
                MediaType.parse("multipart/form-data"), file));
    }

    public void addTXTFile(String name, String filePath, String fileName) {
        this.builder.addFormDataPart(name, fileName, RequestBody.create(
                MediaType.parse("text/plain"), new File(filePath)));
    }

    /**
     * Retrieves WebResponseModel from preference using Request.Builder as key
     *
     * @param request key
     * @return value
     */
    public String getResponse(MultipartBuilder request) {
        try {
            String resp = PreferencesHelper.getValue(new Gson().toJson(request));
            Logger.w("resp cache=" + resp);
            return resp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public void addZipFile(String name, String filePath, String fileName) {
        this.builder.addFormDataPart(name, fileName, RequestBody.create(
                MediaType.parse("application/zip"), new File(filePath)));
    }

    public void execute(String url, final WebServiceListener restClientListener) {
        RequestBody requestBody = null;
        Request request = null;

        requestBody = this.builder.build();
        request = new Request.Builder()
                .addHeader("content-type", "multipart/form-data")
                .url(url).post(requestBody).build();
        try {

            client.interceptors().add(new LoggingInterceptor());
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            /*
                               This condition is here for, when internet is not connected and cached data is not available
                             */
                            try {
                                final String resp = getResponse(builder);
                                if (!Utils.isNetworkConnected(MainApplication.mContext) && resp != null) {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                restClientListener.onSuccess(resp);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } else {
                                    if (Utils.isNetworkConnected(MainApplication.mContext)) {
                                        if (resp != null)
                                            restClientListener.onError("Error receiving data");
                                        else
                                            restClientListener.onError("Server not responding");
                                    } else {
                                        restClientListener.onError("No Internet connection");
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mActivity.get() != null)
                                            restClientListener.onError("on Failure error ");
                                    }
                                });
                            }
                        }
                    });
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    Logger.w("Data=" + response.toString());

                    try {

                        if (!response.isSuccessful()) {

                            Logger.w("response code=" + response.code());
                            Logger.w("response fail=" + response.networkResponse());

                            if (response.code() == 401) {
                                restClientListener.onError(String.valueOf(response.code()));
                            } else {
                                restClientListener.onError(response.toString());
                            }
                            return;
                        }

                        restClientListener.onSuccess(response.body().string());

                    } catch (Exception e) {
                        e.printStackTrace();
                        restClientListener.onError(String.valueOf(response.code()));
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    restClientListener.onError("Error! 2");
                }
            });
        }

    }
}