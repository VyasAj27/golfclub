package com.lp.golfclub.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.golfclub.model.BookingModel;

import java.lang.reflect.Type;
import java.util.List;

/**
 * This class handles all the values which are stored/persisted.
 * @version 1.0.
 */

public class PreferencesHelper {

    public static PreferencesHelper preferencesHelper;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;
    // PREFS
    public static final String PREFS_NAME = "com.lp.golfclub.prefs";

    public static final String PREF_USER_DATA = "PREF_USER_DATA";
    public static final String PREF_LOGIN_SUCCESS = "PREF_LOGIN_SUCCESS";
    public static final String PREF_SOCIAL_LOGIN = "PREF_SOCIAL_LOGIN";
    public static final String PREF_BOOKING_LIST = "PREF_BOOKING_LIST";

    public static PreferencesHelper getInstance(Context context){
        Log.v("TAG" , "instance getInstance");
        if(preferencesHelper == null){
            preferencesHelper = new PreferencesHelper(context);
            Log.v("TAG" , "instance Genereated");
        }
        return preferencesHelper;
    }

    public PreferencesHelper(Context context) {

        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
        this.prefsEditor = sharedPreferences.edit();
    }

    public boolean contains(String key){
        return this.sharedPreferences.contains(key);
    }

    public Long getLong(String key) {
        return sharedPreferences.getLong(key,0);
    }

    public void putLong(String key, Long value) {
        prefsEditor.putLong(key, value);
        prefsEditor.commit();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, ""); // Get our string from prefs or return an empty string
    }

    public void putString(String key, String value) {
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public float getFloat(String key) {
        return sharedPreferences.getFloat(key, 0F);
    }

    public void putFloat(String key, Float value) {
        prefsEditor.putFloat(key, value);
        prefsEditor.commit();
    }

    public void putBoolean(String key, Boolean value) {
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false); // Get our string from prefs or return an false
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public void putInt(String key, int value) {
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }

    public void clear() {
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public void putList(String key, List<BookingModel> data) {
        prefsEditor.putString(key, new Gson().toJson(data));
        prefsEditor.commit();
    }

    public List<BookingModel> getList(String key) {
        Type type = new TypeToken<List<BookingModel>>() {
        }.getType();
        return new Gson().fromJson(preferencesHelper.getString(key), type);
    }

    public void putObject(String key, Object value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        prefsEditor.putString(key, json);
        prefsEditor.commit();
    }
    public Object getObject(String key, Class<?> clazz) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        Object jsonObject = gson.fromJson(json, clazz);
        return jsonObject;
    }


    public static String getValue(String toJson) {
        return preferencesHelper.getString(toJson);
    }
}
