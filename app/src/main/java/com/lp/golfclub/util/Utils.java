package com.lp.golfclub.util;

import android.content.Context;
import android.net.ConnectivityManager;

import com.kaopiz.kprogresshud.KProgressHUD;

public class Utils {

    static KProgressHUD progressDialog;

    public static void showProgressbar(Context context) {
        progressDialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public static void hideProgressbar() {
        if (progressDialog != null) progressDialog.dismiss();
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
