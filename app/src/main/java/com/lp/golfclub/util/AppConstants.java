package com.lp.golfclub.util;

public class AppConstants {

    //table names
    public static final String PARAM_SERVICE = "service";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PROVIDER_ID = "provider_id";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_FIRST_NAME = "firstname";
    public static final String PARAM_LAST_NAME = "lastname";
    public static final String PARAM_PASS_CONFIRM = "password_confirmation";
    public static final String PARAM_MOBILE = "mobile";
    public static final String PARAM_AGE = "age";
    public static final String PARAM_GENDER = "gender";
    public static final String PARAM_PROFILE_IMAGE = "photo";
    public static final String PARAM_ID_PROOF = "photoid";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_MEDIA = "base64_image";
    public static final String PARAM_IMAGE_MIME = "data:image/jpeg;base64,";
    public static final String PARAM_CAT_ID = "cat_id";
    public static final String PARAM_PRODUCT_ID = "product_id";
    public static final String PARAM_BOOKED_FOR_DATE = "booked_for_date";
    public static final String PARAM_IS_DAILY = "is_daily";
    public static final String PARAM_MEMBER_ID = "member_id";
    public static final String PARAM_FB_TOKEN = "fb_token";
    public static final String PARAM_GOOGLE_TOKEN = "google_token";
    public static final String PARAM_MAX_BOOKING_ALLOWED = "max_booking_allowed";
    public static final String PARAM_CLUB_MEMBER_ID = "club_member_id";



    public static final int REQUEST_GOOGLE_SIGN_IN = 1000;


    //server URL
    private static String BASE_URL_DEV = "http://letusgolf.in/dev/";
    private static String BASE_URL_PROD = "http://letusgolf.in/";

    private static String BASE_URL = BASE_URL_DEV +"api/v1/index.php?tag=";
    public static String BASE_URL_IMAGE = BASE_URL_DEV + "images/user_photo/";

    private static String BASE_URL_CLUB_DEV = "https://thecelebrationsportsclub.com/api/";
    private static String BASE_URL_CLUB_PROD = "https://thecelebrationsportsclub.com/api/";

    private static String BASE_URL_CLUB = BASE_URL_CLUB_DEV;

    public static String URL_LOGIN = BASE_URL+ "getlogin";
    public static String URL_SIGN_UP = BASE_URL+ "registerUser";
    public static String URL_FORGOT = BASE_URL+ "forgotPassword";
    public static String URL_RESET = BASE_URL+ "changePassword";
    public static String URL_UPDATE_PROFILE = BASE_URL+ "updateUserProfile";
    public static String URL_USER_DETAIL = BASE_URL+ "getUserDetail";
    public static String URL_UPDATE_TOKEN = BASE_URL+ "updateToken";
    public static String URL_validateRegistrationOTP = BASE_URL+ "validateRegistrationOTP";

    public static String URL_validateOTP = BASE_URL+ "validateOTP";

    public static String URL_GET_PRODUCTS = BASE_URL_CLUB + "index.php?tag=getClubProductsNew&cat_id=28";

    public static String URL_GET_SLOTS = BASE_URL_CLUB + "index.php?tag=getBookingSlotOfDailyProductsNew";
    public static String URL_TERMINOLOGY = BASE_URL_CLUB + "index.php?tag=getTerminologies";
    public static String URL_SUBMIT_SLOT = BASE_URL_CLUB + "index.php?tag=bookClubRooms";
    public static String URL_ADD_MEMBER = BASE_URL_CLUB + "clubAdmin/index.php?tag=addMember";
    public static String URL_BOOKING_HISTORY = BASE_URL_CLUB + "index.php?tag=getClubOrdersByMemeberId";

    public static String URL_BOOK_SLOT_NEW = BASE_URL_CLUB + "index.php?tag=bookDailySlotNew";

    public static String URL_GET_ALL_NOTIFICATION = BASE_URL + "getAllNotificationByUser&user_id=";

}
