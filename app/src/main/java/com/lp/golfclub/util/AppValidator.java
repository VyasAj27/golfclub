package com.lp.golfclub.util;

import android.text.TextUtils;
import android.util.Patterns;

public class AppValidator {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isEmpty(CharSequence target) {
        return (!TextUtils.isEmpty(target));
    }

    private boolean isValidMobile(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }


}
