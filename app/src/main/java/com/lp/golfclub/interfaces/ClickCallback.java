package com.lp.golfclub.interfaces;

public interface ClickCallback {
    // you can define any parameter as per your requirement
    public void onItemClick(String result);

}
