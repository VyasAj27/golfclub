package com.lp.golfclub;

import android.app.Application;
import android.os.StrictMode;

public class MainApplication extends Application {


    public static Application mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}
