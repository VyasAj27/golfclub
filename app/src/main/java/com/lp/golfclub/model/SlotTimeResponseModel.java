package com.lp.golfclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotTimeResponseModel {

    @SerializedName("slot_id")
    @Expose
    private String slotId;
    @SerializedName("already_booked")
    @Expose
    private Integer alreadyBooked;
    @SerializedName("slot_title")
    @Expose
    private String slotTitle;
    @SerializedName("total_booking_done")
    @Expose
    private Integer totalBookingDone;
    @SerializedName("booking_allowed")
    @Expose
    private Integer bookingAllowed;
    @SerializedName("in_process")
    @Expose
    private Integer inProcess;
    @SerializedName("remaining_booking")
    @Expose
    private Integer remainingBooking;

    private boolean isSelected;

    public Integer getRemainingBooking() {
        return remainingBooking;
    }

    public void setRemainingBooking(Integer remainingBooking) {
        this.remainingBooking = remainingBooking;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public Integer getAlreadyBooked() {
        return alreadyBooked;
    }

    public void setAlreadyBooked(Integer alreadyBooked) {
        this.alreadyBooked = alreadyBooked;
    }

    public String getSlotTitle() {
        return slotTitle;
    }

    public void setSlotTitle(String slotTitle) {
        this.slotTitle = slotTitle;
    }

    public Integer getTotalBookingDone() {
        return totalBookingDone;
    }

    public void setTotalBookingDone(Integer totalBookingDone) {
        this.totalBookingDone = totalBookingDone;
    }

    public Integer getBookingAllowed() {
        return bookingAllowed;
    }

    public void setBookingAllowed(Integer bookingAllowed) {
        this.bookingAllowed = bookingAllowed;
    }

    public Integer getInProcess() {
        return inProcess;
    }

    public void setInProcess(Integer inProcess) {
        this.inProcess = inProcess;
    }

}
