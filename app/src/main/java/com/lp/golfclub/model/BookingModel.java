package com.lp.golfclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("increment_id")
    @Expose
    private String incrementId;
    @SerializedName("ordering")
    @Expose
    private String ordering;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("checked_out")
    @Expose
    private String checkedOut;
    @SerializedName("checked_out_time")
    @Expose
    private String checkedOutTime;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("modified_by")
    @Expose
    private String modifiedBy;
    @SerializedName("member_id")
    @Expose
    private String memberId;
    @SerializedName("ordered_for")
    @Expose
    private String orderedFor;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("order_member_id")
    @Expose
    private String orderMemberId;
    @SerializedName("no_of_member")
    @Expose
    private String noOfMember;
    @SerializedName("order_base_amt")
    @Expose
    private String orderBaseAmt;
    @SerializedName("order_gst_amt")
    @Expose
    private String orderGstAmt;
    @SerializedName("order_total")
    @Expose
    private String orderTotal;
    @SerializedName("convenience_fee")
    @Expose
    private String convenienceFee;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("is_customer_notified")
    @Expose
    private String isCustomerNotified;
    @SerializedName("is_updated")
    @Expose
    private String isUpdated;
    @SerializedName("no_of_guest")
    @Expose
    private String noOfGuest;
    @SerializedName("guest_gst_amt")
    @Expose
    private String guestGstAmt;
    @SerializedName("guest_base_amt")
    @Expose
    private String guestBaseAmt;
    @SerializedName("guest_total")
    @Expose
    private String guestTotal;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("cheque_card_number")
    @Expose
    private String chequeCardNumber;
    @SerializedName("guest_fee_gst_amt")
    @Expose
    private String guestFeeGstAmt;
    @SerializedName("guest_fee_base_amt")
    @Expose
    private String guestFeeBaseAmt;
    @SerializedName("guest_fee_total")
    @Expose
    private String guestFeeTotal;
    @SerializedName("coaching_timing")
    @Expose
    private String coachingTiming;
    @SerializedName("is_coaching")
    @Expose
    private String isCoaching;
    @SerializedName("accessories")
    @Expose
    private String accessories;
    @SerializedName("accessories_prices")
    @Expose
    private String accessoriesPrices;
    @SerializedName("is_couple")
    @Expose
    private String isCouple;
    @SerializedName("guest_order_id")
    @Expose
    private String guestOrderId;
    @SerializedName("is_member_playing")
    @Expose
    private String isMemberPlaying;
    @SerializedName("no_of_extra_bed")
    @Expose
    private String noOfExtraBed;
    @SerializedName("extra_bed_price")
    @Expose
    private String extraBedPrice;
    @SerializedName("extra_bed_price_gst")
    @Expose
    private String extraBedPriceGst;
    @SerializedName("order_status_title")
    @Expose
    private String orderStatusTitle;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_type_name")
    @Expose
    private String productTypeName;
    @SerializedName("product_plan_type_name")
    @Expose
    private String productPlanTypeName;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("booked_from_date")
    @Expose
    private String bookedFromDate;
    @SerializedName("booked_to_date")
    @Expose
    private String bookedToDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getOrdering() {
        return ordering;
    }

    public void setOrdering(String ordering) {
        this.ordering = ordering;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(String checkedOut) {
        this.checkedOut = checkedOut;
    }

    public String getCheckedOutTime() {
        return checkedOutTime;
    }

    public void setCheckedOutTime(String checkedOutTime) {
        this.checkedOutTime = checkedOutTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderedFor() {
        return orderedFor;
    }

    public void setOrderedFor(String orderedFor) {
        this.orderedFor = orderedFor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOrderMemberId() {
        return orderMemberId;
    }

    public void setOrderMemberId(String orderMemberId) {
        this.orderMemberId = orderMemberId;
    }

    public String getNoOfMember() {
        return noOfMember;
    }

    public void setNoOfMember(String noOfMember) {
        this.noOfMember = noOfMember;
    }

    public String getOrderBaseAmt() {
        return orderBaseAmt;
    }

    public void setOrderBaseAmt(String orderBaseAmt) {
        this.orderBaseAmt = orderBaseAmt;
    }

    public String getOrderGstAmt() {
        return orderGstAmt;
    }

    public void setOrderGstAmt(String orderGstAmt) {
        this.orderGstAmt = orderGstAmt;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getConvenienceFee() {
        return convenienceFee;
    }

    public void setConvenienceFee(String convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getIsCustomerNotified() {
        return isCustomerNotified;
    }

    public void setIsCustomerNotified(String isCustomerNotified) {
        this.isCustomerNotified = isCustomerNotified;
    }

    public String getIsUpdated() {
        return isUpdated;
    }

    public void setIsUpdated(String isUpdated) {
        this.isUpdated = isUpdated;
    }

    public String getNoOfGuest() {
        return noOfGuest;
    }

    public void setNoOfGuest(String noOfGuest) {
        this.noOfGuest = noOfGuest;
    }

    public String getGuestGstAmt() {
        return guestGstAmt;
    }

    public void setGuestGstAmt(String guestGstAmt) {
        this.guestGstAmt = guestGstAmt;
    }

    public String getGuestBaseAmt() {
        return guestBaseAmt;
    }

    public void setGuestBaseAmt(String guestBaseAmt) {
        this.guestBaseAmt = guestBaseAmt;
    }

    public String getGuestTotal() {
        return guestTotal;
    }

    public void setGuestTotal(String guestTotal) {
        this.guestTotal = guestTotal;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getChequeCardNumber() {
        return chequeCardNumber;
    }

    public void setChequeCardNumber(String chequeCardNumber) {
        this.chequeCardNumber = chequeCardNumber;
    }

    public String getGuestFeeGstAmt() {
        return guestFeeGstAmt;
    }

    public void setGuestFeeGstAmt(String guestFeeGstAmt) {
        this.guestFeeGstAmt = guestFeeGstAmt;
    }

    public String getGuestFeeBaseAmt() {
        return guestFeeBaseAmt;
    }

    public void setGuestFeeBaseAmt(String guestFeeBaseAmt) {
        this.guestFeeBaseAmt = guestFeeBaseAmt;
    }

    public String getGuestFeeTotal() {
        return guestFeeTotal;
    }

    public void setGuestFeeTotal(String guestFeeTotal) {
        this.guestFeeTotal = guestFeeTotal;
    }

    public String getCoachingTiming() {
        return coachingTiming;
    }

    public void setCoachingTiming(String coachingTiming) {
        this.coachingTiming = coachingTiming;
    }

    public String getIsCoaching() {
        return isCoaching;
    }

    public void setIsCoaching(String isCoaching) {
        this.isCoaching = isCoaching;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }

    public String getAccessoriesPrices() {
        return accessoriesPrices;
    }

    public void setAccessoriesPrices(String accessoriesPrices) {
        this.accessoriesPrices = accessoriesPrices;
    }

    public String getIsCouple() {
        return isCouple;
    }

    public void setIsCouple(String isCouple) {
        this.isCouple = isCouple;
    }

    public String getGuestOrderId() {
        return guestOrderId;
    }

    public void setGuestOrderId(String guestOrderId) {
        this.guestOrderId = guestOrderId;
    }

    public String getIsMemberPlaying() {
        return isMemberPlaying;
    }

    public void setIsMemberPlaying(String isMemberPlaying) {
        this.isMemberPlaying = isMemberPlaying;
    }

    public String getNoOfExtraBed() {
        return noOfExtraBed;
    }

    public void setNoOfExtraBed(String noOfExtraBed) {
        this.noOfExtraBed = noOfExtraBed;
    }

    public String getExtraBedPrice() {
        return extraBedPrice;
    }

    public void setExtraBedPrice(String extraBedPrice) {
        this.extraBedPrice = extraBedPrice;
    }

    public String getExtraBedPriceGst() {
        return extraBedPriceGst;
    }

    public void setExtraBedPriceGst(String extraBedPriceGst) {
        this.extraBedPriceGst = extraBedPriceGst;
    }

    public String getOrderStatusTitle() {
        return orderStatusTitle;
    }

    public void setOrderStatusTitle(String orderStatusTitle) {
        this.orderStatusTitle = orderStatusTitle;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getProductPlanTypeName() {
        return productPlanTypeName;
    }

    public void setProductPlanTypeName(String productPlanTypeName) {
        this.productPlanTypeName = productPlanTypeName;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBookedFromDate() {
        return bookedFromDate;
    }

    public void setBookedFromDate(String bookedFromDate) {
        this.bookedFromDate = bookedFromDate;
    }

    public String getBookedToDate() {
        return bookedToDate;
    }

    public void setBookedToDate(String bookedToDate) {
        this.bookedToDate = bookedToDate;
    }

}