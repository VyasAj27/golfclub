package com.lp.golfclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("product_type")
    @Expose
    private String productType;
    @SerializedName("product_plan_type")
    @Expose
    private String productPlanType;
    @SerializedName("customdates")
    @Expose
    private String customdates;
    @SerializedName("base_member_price")
    @Expose
    private String baseMemberPrice;
    @SerializedName("member_gst_price")
    @Expose
    private String memberGstPrice;
    @SerializedName("member_price")
    @Expose
    private String memberPrice;
    @SerializedName("is_allowed_non_member")
    @Expose
    private String isAllowedNonMember;
    @SerializedName("base_non_member_price")
    @Expose
    private String baseNonMemberPrice;
    @SerializedName("non_member_gst_price")
    @Expose
    private String nonMemberGstPrice;
    @SerializedName("non_member_price")
    @Expose
    private String nonMemberPrice;
    @SerializedName("guest_fee")
    @Expose
    private String guestFee;
    @SerializedName("guest_fee_after")
    @Expose
    private String guestFeeAfter;
    @SerializedName("new_guest_fee")
    @Expose
    private String newGuestFee;
    @SerializedName("is_taxable")
    @Expose
    private String isTaxable;
    @SerializedName("tax_type")
    @Expose
    private String taxType;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("product_plan_type_title")
    @Expose
    private String productPlanTypeTitle;
    @SerializedName("product_type_title")
    @Expose
    private String productTypeTitle;
    @SerializedName("max_booking_allowed")
    @Expose
    private String maxBookingAllowed;
    @SerializedName("booking_count")
    @Expose
    private String bookingCount;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("is_coaching")
    @Expose
    private String isCoaching;
    @SerializedName("coaching_timing")
    @Expose
    private String coachingTiming;
    @SerializedName("allow_accessories")
    @Expose
    private String allowAccessories;
    @SerializedName("accessories_prices")
    @Expose
    private String accessoriesPrices;
    @SerializedName("is_couple")
    @Expose
    private String isCouple;
    @SerializedName("is_product_is_event")
    @Expose
    private String isProductIsEvent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductPlanType() {
        return productPlanType;
    }

    public void setProductPlanType(String productPlanType) {
        this.productPlanType = productPlanType;
    }

    public String getCustomdates() {
        return customdates;
    }

    public void setCustomdates(String customdates) {
        this.customdates = customdates;
    }

    public String getBaseMemberPrice() {
        return baseMemberPrice;
    }

    public void setBaseMemberPrice(String baseMemberPrice) {
        this.baseMemberPrice = baseMemberPrice;
    }

    public String getMemberGstPrice() {
        return memberGstPrice;
    }

    public void setMemberGstPrice(String memberGstPrice) {
        this.memberGstPrice = memberGstPrice;
    }

    public String getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(String memberPrice) {
        this.memberPrice = memberPrice;
    }

    public String getIsAllowedNonMember() {
        return isAllowedNonMember;
    }

    public void setIsAllowedNonMember(String isAllowedNonMember) {
        this.isAllowedNonMember = isAllowedNonMember;
    }

    public String getBaseNonMemberPrice() {
        return baseNonMemberPrice;
    }

    public void setBaseNonMemberPrice(String baseNonMemberPrice) {
        this.baseNonMemberPrice = baseNonMemberPrice;
    }

    public String getNonMemberGstPrice() {
        return nonMemberGstPrice;
    }

    public void setNonMemberGstPrice(String nonMemberGstPrice) {
        this.nonMemberGstPrice = nonMemberGstPrice;
    }

    public String getNonMemberPrice() {
        return nonMemberPrice;
    }

    public void setNonMemberPrice(String nonMemberPrice) {
        this.nonMemberPrice = nonMemberPrice;
    }

    public String getGuestFee() {
        return guestFee;
    }

    public void setGuestFee(String guestFee) {
        this.guestFee = guestFee;
    }

    public String getGuestFeeAfter() {
        return guestFeeAfter;
    }

    public void setGuestFeeAfter(String guestFeeAfter) {
        this.guestFeeAfter = guestFeeAfter;
    }

    public String getNewGuestFee() {
        return newGuestFee;
    }

    public void setNewGuestFee(String newGuestFee) {
        this.newGuestFee = newGuestFee;
    }

    public String getIsTaxable() {
        return isTaxable;
    }

    public void setIsTaxable(String isTaxable) {
        this.isTaxable = isTaxable;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getProductPlanTypeTitle() {
        return productPlanTypeTitle;
    }

    public void setProductPlanTypeTitle(String productPlanTypeTitle) {
        this.productPlanTypeTitle = productPlanTypeTitle;
    }

    public String getProductTypeTitle() {
        return productTypeTitle;
    }

    public void setProductTypeTitle(String productTypeTitle) {
        this.productTypeTitle = productTypeTitle;
    }

    public String getMaxBookingAllowed() {
        return maxBookingAllowed;
    }

    public void setMaxBookingAllowed(String maxBookingAllowed) {
        this.maxBookingAllowed = maxBookingAllowed;
    }

    public String getBookingCount() {
        return bookingCount;
    }

    public void setBookingCount(String bookingCount) {
        this.bookingCount = bookingCount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIsCoaching() {
        return isCoaching;
    }

    public void setIsCoaching(String isCoaching) {
        this.isCoaching = isCoaching;
    }

    public String getCoachingTiming() {
        return coachingTiming;
    }

    public void setCoachingTiming(String coachingTiming) {
        this.coachingTiming = coachingTiming;
    }

    public String getAllowAccessories() {
        return allowAccessories;
    }

    public void setAllowAccessories(String allowAccessories) {
        this.allowAccessories = allowAccessories;
    }

    public String getAccessoriesPrices() {
        return accessoriesPrices;
    }

    public void setAccessoriesPrices(String accessoriesPrices) {
        this.accessoriesPrices = accessoriesPrices;
    }

    public String getIsCouple() {
        return isCouple;
    }

    public void setIsCouple(String isCouple) {
        this.isCouple = isCouple;
    }

    public String getIsProductIsEvent() {
        return isProductIsEvent;
    }

    public void setIsProductIsEvent(String isProductIsEvent) {
        this.isProductIsEvent = isProductIsEvent;
    }

}