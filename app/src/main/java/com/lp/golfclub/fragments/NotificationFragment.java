package com.lp.golfclub.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.adapter.NotificationAdapter;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.NotificationList;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class NotificationFragment extends Fragment {

    @BindView(R.id.rcNotifications)
    RecyclerView rcNotifications;


    private ArrayList<NotificationList> alNotifications = new ArrayList<>();
    private NotificationAdapter mAdapter;
    private UserData userData;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Notifications");
    }

    private void getNotifications() {

        getActivity().runOnUiThread(() -> Utils.showProgressbar(getActivity()));

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();

        mHelper.post(AppConstants.URL_GET_ALL_NOTIFICATION + userData.getId(), new WebServiceHelper.WebServiceListener() {
            @Override
            public void onSuccess(final String response) {

                getActivity().runOnUiThread(() -> Utils.hideProgressbar());

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    Type listType = new TypeToken<List<NotificationList>>() {
                    }.getType();
                    List<NotificationList> notifs = new Gson().fromJson(webResponseModel.getData(), listType);

                    alNotifications.clear();
                    alNotifications.addAll(notifs);

                    getActivity().runOnUiThread(() -> mAdapter.notifyDataSetChanged());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {
                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_GET);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.notification_fragment, container, false);
        ButterKnife.bind(this, view);

        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);
        initTutorialAdapter();
        return view;
    }

    private void initTutorialAdapter() {
        getNotifications();
        mAdapter = new NotificationAdapter(alNotifications, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rcNotifications.setLayoutManager(mLayoutManager);
        rcNotifications.setItemAnimator(new DefaultItemAnimator());
        rcNotifications.setAdapter(mAdapter);
    }

}
