package com.lp.golfclub.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class ContactUsFragment extends Fragment {

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.etUserName)
    AppCompatEditText etUserName;

    @BindView(R.id.etEmail)
    AppCompatEditText etEmail;

    @BindView(R.id.etMobile)
    AppCompatEditText etMobile;

    @BindView(R.id.etMessage)
    AppCompatEditText etMessage;

    public ContactUsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Contact Us");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.contact_us_fragment, container, false);
        ButterKnife.bind(this, view);

        UserData userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        etUserName.setText(userData.getName());
        etEmail.setText(userData.getEmail());
        etMobile.setText(userData.getMobile());

        tvSubmit.setOnClickListener(v -> {
            if (!AppValidator.isEmpty(etMessage.getText().toString())) {
                Toast.makeText(getActivity(), "Enter message.", Toast.LENGTH_SHORT).show();
            } else {
                etMessage.setText("");
                Toast.makeText(getActivity(), "Thanks for contacting us, our expert will get back to you soon.", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
