package com.lp.golfclub.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.model.BookingModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
@SuppressLint("ValidFragment")
public class HistoryDetailFragment extends Fragment {

    private BookingModel bookingModel;

    @BindView(R.id.tvOrderId)
    AppCompatTextView tvOrderId;

    @BindView(R.id.tvDate)
    AppCompatTextView tvDate;

    @BindView(R.id.tvBookingDate)
    AppCompatTextView tvBookingDate;

    @BindView(R.id.tvMode)
    AppCompatTextView tvMode;

    @BindView(R.id.tvRupee)
    AppCompatTextView tvRupee;

    @BindView(R.id.tvOrderBy)
    AppCompatTextView tvOrderBy;

    public HistoryDetailFragment(BookingModel bookingModel) {
        // Required empty public constructor
        this.bookingModel = bookingModel;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Booking Details");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.history_detail_fragment, container, false);
        ButterKnife.bind(this, view);

        tvOrderId.setText(bookingModel.getIncrementId());
        tvDate.setText(bookingModel.getBookedToDate() + " \n" + bookingModel.getCoachingTiming());
        tvBookingDate.setText("Booked on: "+bookingModel.getBookedFromDate());
        tvMode.setText("Paid by "+bookingModel.getPaymentType());
        tvRupee.setText("\u20B9"+bookingModel.getGuestTotal());

        tvOrderBy.setText("Ordered by : "+bookingModel.getName());

        return view;
    }

}
