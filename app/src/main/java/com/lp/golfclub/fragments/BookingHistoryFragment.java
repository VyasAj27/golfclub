package com.lp.golfclub.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.adapter.HistoryAdapter;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.BookingModel;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class BookingHistoryFragment extends Fragment {

    @BindView(R.id.rcHistory)
    RecyclerView rcHistory;

    @BindView(R.id.tvNoData)
    AppCompatTextView tvNoData;

    private HistoryAdapter mAdapter;
    private List<BookingModel> alBookings;
    private UserData userData;

    public BookingHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Booking History");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alBookings = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.history_fragment, container, false);

        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        ButterKnife.bind(this, view);
        initTutorialAdapter();
        return view;
    }

    private void initTutorialAdapter() {
        if (TextUtils.isEmpty(userData.getClubMemberId())) {
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            if (alBookings.isEmpty()) {
                getBookingHistory();
            }
        }

        mAdapter = new HistoryAdapter(alBookings, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rcHistory.setLayoutManager(mLayoutManager);
        rcHistory.setItemAnimator(new DefaultItemAnimator());
        rcHistory.setAdapter(mAdapter);
    }

    private void getBookingHistory() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("is_golf_req", "1");
        mHelper.addFormParam(AppConstants.PARAM_MEMBER_ID, userData.getClubMemberId());

        mHelper.postFormData(AppConstants.URL_BOOKING_HISTORY, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {


                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    Log.v("TAG", "Get Data::::" + response);
                    alBookings.clear();
                    Type listType = new TypeToken<List<BookingModel>>() {
                    }.getType();

                    if(mJsonObject.has("order")){
                        JSONArray slots = mJsonObject.getJSONArray("order");
                        alBookings.addAll(new Gson().fromJson(slots.toString(), listType));
                    }

                    getActivity().runOnUiThread(() -> {
                        Utils.hideProgressbar();

                        if (alBookings.isEmpty()) {
                            tvNoData.setVisibility(View.VISIBLE);
                        } else {
                            tvNoData.setVisibility(View.GONE);
                        }

                        mAdapter.notifyDataSetChanged();
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

}
