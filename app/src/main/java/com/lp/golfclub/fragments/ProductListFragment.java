package com.lp.golfclub.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.adapter.ProductsAdapter;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.ProductModel;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class ProductListFragment extends Fragment {

    @BindView(R.id.rcProductList)
    RecyclerView rcProductList;

    private ArrayList<ProductModel> alProducts = new ArrayList<>();

    private ProductsAdapter productsAdapter;

    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Select Product");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.select_product_fragment, container, false);
        ButterKnife.bind(this, view);

        initTutorialAdapter();

        return view;
    }

    private void initTutorialAdapter() {
        if(alProducts.isEmpty())getProducts();
        productsAdapter = new ProductsAdapter(alProducts, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rcProductList.setLayoutManager(mLayoutManager);
        rcProductList.setItemAnimator(new DefaultItemAnimator());
        rcProductList.setAdapter(productsAdapter);
    }

    private void getProducts() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();

        mHelper.post(AppConstants.URL_GET_PRODUCTS, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    JSONArray products = mJsonObject.getJSONArray("products");

                    getActivity().runOnUiThread(() -> {
                        Type listType = new TypeToken<List<ProductModel>>() {
                        }.getType();
                        List<ProductModel> posts = new Gson().fromJson(products.toString(), listType);
                        alProducts.clear();
                        for (int i = 0; i < posts.size(); i++) {
                            if (!posts.get(i).getProductPlanTypeTitle().equals("Monthly")) {
                                alProducts.add(posts.get(i));
                            }
                        }
                        productsAdapter.notifyDataSetChanged();
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {
                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_GET);
    }

}
