package com.lp.golfclub.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.BookingModel;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.tvEditProfile)
    AppCompatTextView tvEditProfile;

    @BindView(R.id.tvMobile)
    AppCompatTextView tvMobile;

    @BindView(R.id.tvEmail)
    AppCompatTextView tvEmail;

    @BindView(R.id.tvName)
    AppCompatTextView tvName;

    @BindView(R.id.tvDetails)
    AppCompatTextView tvDetails;

    @BindView(R.id.tvBooking)
    AppCompatTextView tvBooking;

    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;

    @BindView(R.id.pbLoader)
    ProgressBar pbLoader;

    private List<BookingModel> alBookings = new ArrayList<>();

    private UserData userData;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("My Profile");
        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);
        updateUserData();
    }

    private void updateUserData() {
        pbLoader.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(AppConstants.BASE_URL_IMAGE + userData.getProfileImage())
                .placeholder(R.drawable.ic_user_profile_photo)
                .centerCrop()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        pbLoader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        pbLoader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(ivProfile);
        tvMobile.setText(userData.getMobile());
        tvEmail.setText(userData.getEmail());
        tvName.setText(userData.getName() +" "+ userData.getLastname());
        tvDetails.setText(userData.getGender());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.my_profile_fragment, container, false);
        ButterKnife.bind(this, view);

        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        if (TextUtils.isEmpty(userData.getClubMemberId())) {
            tvBooking.setText("0");
        } else {
            getBookingHistory();
        }

        tvEditProfile.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvEditProfile:
                ((MainActivity) getActivity()).loadNewBooking(new EditMyProfileFragment());
                break;
        }
    }

    private void getBookingHistory() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("is_golf_req", "1");
        mHelper.addFormParam(AppConstants.PARAM_MEMBER_ID, userData.getClubMemberId());

        mHelper.postFormData(AppConstants.URL_BOOKING_HISTORY, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {


                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    Log.v("TAG", "Get Data::::" + response);
                    Type listType = new TypeToken<List<BookingModel>>() {
                    }.getType();
                    if(mJsonObject.has("order")){
                        JSONArray slots = mJsonObject.getJSONArray("order");
                        alBookings.clear();
                        alBookings.addAll(new Gson().fromJson(slots.toString(), listType));
                    }

                    getActivity().runOnUiThread(() -> {
                        Utils.hideProgressbar();

                        tvBooking.setText(alBookings.size() + "");
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> Utils.hideProgressbar());
            }
        }, WebServiceHelper.METHOD_POST);
    }
}
