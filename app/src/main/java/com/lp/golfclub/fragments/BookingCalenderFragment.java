package com.lp.golfclub.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.adapter.CalanderSlotAdapter;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.ProductModel;
import com.lp.golfclub.model.SlotTimeResponseModel;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
@SuppressLint("ValidFragment")
public class BookingCalenderFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.tvBookMySlot)
    AppCompatTextView tvBookMySlot;

    @BindView(R.id.rvSlots)
    RecyclerView rvSlots;

    private ArrayList<SlotTimeResponseModel> alSlots;
    private CalanderSlotAdapter mAdapter;
    public Calendar calendar;
    private ProductModel productModel;
    String myFormat = "yyyy-MM-dd"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    private UserData userData;

    @SuppressLint("ValidFragment")
    public BookingCalenderFragment(ProductModel productModel) {
        // Required empty public constructor
        this.productModel = productModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Slot Booking");
    }

    private void getBookingSlots() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam(AppConstants.PARAM_CAT_ID, productModel.getCategoryId());
        mHelper.addFormParam(AppConstants.PARAM_PRODUCT_ID, productModel.getId());
        mHelper.addFormParam(AppConstants.PARAM_BOOKED_FOR_DATE, sdf.format(calendar.getTime()));
        mHelper.addFormParam(AppConstants.PARAM_IS_DAILY, "1");
        mHelper.addFormParam(AppConstants.PARAM_MAX_BOOKING_ALLOWED, "10");
        mHelper.addFormParam(AppConstants.PARAM_MEMBER_ID, userData.getClubMemberId());

        mHelper.postFormData(AppConstants.URL_GET_SLOTS, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {


                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());

                    Log.v("TAG", "Get Data::::" + response);
                    alSlots.clear();
                    Type listType = new TypeToken<List<SlotTimeResponseModel>>() {
                    }.getType();
                    JSONArray slots = mJsonObject.getJSONArray("slots");
                    alSlots.addAll(new Gson().fromJson(slots.toString(), listType));

                    getActivity().runOnUiThread(() -> {
                        Utils.hideProgressbar();
                        mAdapter.notifyDataSetChanged();
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void addMember() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("name", userData.getName());
        mHelper.addFormParam("email", userData.getEmail());
        mHelper.addFormParam("mobile", userData.getMobile());
        mHelper.addFormParam("membership_type", "NM");
        mHelper.addFormParam("member_type", "NONMEMBER");
        mHelper.addFormParam("address", "");
        mHelper.addFormParam("dob", "");
        mHelper.addFormParam("created_by", userData.getId().toString());

        mHelper.postFormData(AppConstants.URL_ADD_MEMBER, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);
                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());
                    Log.v("TAG", "Get Data::::" + response);
                    JSONObject user = mJsonObject.getJSONObject("user");
                    userData.setClubMemberId(user.getString("user_id"));
                    PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);
                    getActivity().runOnUiThread(() -> {
                        Utils.hideProgressbar();
                    });
                    updateProfile();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    private void updateProfile() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });
        final WebServiceHelper mHelper = WebServiceHelper.getInstance();

        mHelper.addFormParam("user_id", userData.getId().toString());
        mHelper.addFormParam(AppConstants.PARAM_FIRST_NAME, userData.getName());
        mHelper.addFormParam(AppConstants.PARAM_LAST_NAME, userData.getName());
        mHelper.addFormParam(AppConstants.PARAM_EMAIL, userData.getEmail());
        mHelper.addFormParam(AppConstants.PARAM_MOBILE, userData.getMobile());
        mHelper.addFormParam(AppConstants.PARAM_AGE, userData.getAge());
        mHelper.addFormParam(AppConstants.PARAM_GENDER, userData.getGender());
        mHelper.addFormParam(AppConstants.PARAM_PROFILE_IMAGE, userData.getProfileImage());
        mHelper.addFormParam(AppConstants.PARAM_ID_PROOF, userData.getIdProof());
        mHelper.addParam("club_member_id", userData.getClubMemberId());

        mHelper.postFormData(AppConstants.URL_UPDATE_PROFILE, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                if (TextUtils.isEmpty(response)) return;

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });

                getBookingSlots();

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alSlots = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.booking_calander_fragment, container, false);
        ButterKnife.bind(this, view);

        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        tvBookMySlot.setOnClickListener(this);

        Calendar startDate = Calendar.getInstance();
        calendar = startDate;
        startDate.add(Calendar.DAY_OF_MONTH, 0);
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 2);
        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
                super.onCalendarScroll(calendarView, dx, dy);
                if (sdf.format(horizontalCalendar.getSelectedDate().getTime()).equals(sdf.format(Calendar.getInstance().getTime()))) {
                    calendar = horizontalCalendar.getSelectedDate();
                    mAdapter.setCalanderDate(calendar);
                    callBooking();
                    Log.v("TAG", "Current Selected onCalendarScroll");
                }
            }

            @Override
            public void onDateSelected(Calendar date, int position) {
                calendar = date;
                mAdapter.setCalanderDate(calendar);
                callBooking();
                Log.v("TAG", "Current Selected date" + date.getTime());
            }
        });
        initTutorialAdapter();
        mAdapter.setCalanderDate(calendar);


        return view;
    }

    private void callBooking(){
        if (TextUtils.isEmpty(userData.getClubMemberId()) || userData.getClubMemberId().equals("0")) {
            addMember();
        } else {
            getBookingSlots();
        }
    }

    private void initTutorialAdapter() {
        mAdapter = new CalanderSlotAdapter(alSlots, getActivity());
        int numberOfColumns = 2;
        rvSlots.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        rvSlots.setItemAnimator(new DefaultItemAnimator());
        rvSlots.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvBookMySlot:
                List<SlotTimeResponseModel> currentSelectedTabs = mAdapter.getCurrentSelectedTabs();
                if (currentSelectedTabs.isEmpty()) {
                    Toast.makeText(getActivity(), "Select any slot for booking.", Toast.LENGTH_SHORT).show();
                } else {
                    ((MainActivity) getActivity()).loadNewBooking(new NewBookingFragment(calendar, currentSelectedTabs, productModel));
                }
                break;
        }
    }
}
