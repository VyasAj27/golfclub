package com.lp.golfclub.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.ProductModel;
import com.lp.golfclub.model.SlotTimeResponseModel;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
@SuppressLint("ValidFragment")
public class NewBookingFragment extends Fragment {

    @BindView(R.id.tilBooking)
    TextInputLayout tilBooking;

    @BindView(R.id.etBooking)
    AppCompatEditText etBooking;

    @BindView(R.id.tilSlotTiming)
    TextInputLayout tilSlotTiming;

    @BindView(R.id.etSlotTiming)
    AppCompatEditText etSlotTiming;

    @BindView(R.id.etUserName)
    AppCompatEditText etUserName;

    @BindView(R.id.etEmail)
    AppCompatEditText etEmail;

    @BindView(R.id.etPhone)
    AppCompatEditText etPhone;

    @BindView(R.id.etPaymentType)
    AppCompatEditText etPaymentType;

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.tvPrice)
    AppCompatTextView tvPrice;

    @BindView(R.id.tvAmountBreakup)
    AppCompatTextView tvAmountBreakup;

    @BindView(R.id.tvAmountBreakupValue)
    AppCompatTextView tvAmountBreakupValue;

    Calendar myCalendar = Calendar.getInstance();

    List<SlotTimeResponseModel> currentSelectedTabs = new ArrayList<>();

    String myFormat = "MMM , dd/yyyy"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    private ProductModel productModel;
    private UserData userData;

    @SuppressLint("ValidFragment")
    public NewBookingFragment(Calendar calendar, List<SlotTimeResponseModel> currentSelectedTabs, ProductModel productModel) {
        // Required empty public constructor
        myCalendar = calendar;
        this.currentSelectedTabs.addAll(currentSelectedTabs);
        this.productModel = productModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Booking Page");
    }

    private boolean validate() {
        if (!AppValidator.isEmpty(etUserName.getText())) {
            Toast.makeText(getActivity(), "Enter valid username", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!AppValidator.isValidEmail(etEmail.getText())) {
            Toast.makeText(getActivity(), "Enter valid email", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!AppValidator.isEmpty(etPhone.getText())) {
            Toast.makeText(getActivity(), "Enter mobile number", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etPhone.getText().toString().length() != 10) {
            Toast.makeText(getActivity(), "Mobile number should be of 10 digit.", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!AppValidator.isEmpty(etPaymentType.getText())) {
            Toast.makeText(getActivity(), "Enter Payment Type", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_booking, container, false);
        ButterKnife.bind(this, view);

        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);

        etUserName.setText(userData.getName());
        etEmail.setText(userData.getEmail());
        etPhone.setText(userData.getMobile());
        etPaymentType.setText("Cash");

        etBooking.setText(sdf.format(myCalendar.getTime()));
        tvPrice.setText("\u20B9 " + productModel.getNonMemberPrice() + " (incl. of taxes)");
        etSlotTiming.setText(currentSelectedTabs.get(0).getSlotTitle());

        tvAmountBreakup.setOnClickListener(view1 -> {
            tvAmountBreakupValue.setVisibility(View.VISIBLE);
            tvAmountBreakupValue.setText("Base price \u20B9 " + productModel.getBaseNonMemberPrice() + "\n" + "Gst price \u20B9 " + productModel.getNonMemberGstPrice());
        });

        tvSubmit.setOnClickListener(v -> {
            if (validate()) {
                submitBooking();
            }
        });

        return view;
    }

    private void submitBooking() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat mySdf = new SimpleDateFormat(myFormat, Locale.US);

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();
        mHelper.addFormParam("member_id", userData.getClubMemberId());
        mHelper.addFormParam("ordered_for", "SELF");
        mHelper.addFormParam("name", userData.getName());
        mHelper.addFormParam("email", userData.getEmail());
        mHelper.addFormParam("mobile", userData.getMobile());
        mHelper.addFormParam("order_member_id", userData.getClubMemberId());
        mHelper.addFormParam(AppConstants.PARAM_PRODUCT_ID, productModel.getId());
        mHelper.addFormParam("booked_from_date", mySdf.format(myCalendar.getTime()));
        mHelper.addFormParam("booked_to_date", mySdf.format(myCalendar.getTime()));
        mHelper.addFormParam("order_comment", "");
        mHelper.addFormParam("is_daily", "1");
        mHelper.addFormParam("category_id", productModel.getCategoryId());
        mHelper.addFormParam("max_booking_allowed", productModel.getMaxBookingAllowed());
        mHelper.addFormParam("repeat_event_id", currentSelectedTabs.get(0).getSlotId());
        mHelper.addFormParam("booking_count", "1");
        mHelper.addFormParam("payment_type", "Cash");
        mHelper.addFormParam("created_by", userData.getId().toString());
        mHelper.addFormParam("convenience_fee", "0");
        mHelper.addFormParam("is_admin", "0");
        mHelper.addFormParam("total_price", productModel.getNonMemberPrice());
        mHelper.addFormParam("no_of_guest", "1");
        mHelper.addFormParam("cheque_card_number", "");
        mHelper.addFormParam("bank_name", "");
        mHelper.addFormParam("is_already_booked", "0");
        mHelper.addFormParam("is_member_playing", "1");
        mHelper.addFormParam("include_guest_fee", "1");
        mHelper.addFormParam("member_locker_count", "0");
        mHelper.addFormParam("nonmember_locker_count", "0");
        mHelper.addFormParam("member_towel_count", "0");
        mHelper.addFormParam("nonmember_towel_count", "0");
        mHelper.addFormParam("guest_order_id", "0");
        mHelper.addFormParam("no_of_member", "0");

        mHelper.postFormData(AppConstants.URL_BOOK_SLOT_NEW, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);

                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getActivity(), "Thank you for your booking!", Toast.LENGTH_SHORT).show();
                        Utils.hideProgressbar();
                        ((MainActivity) getActivity()).loadBookingHistory(new BookingHistoryFragment());
                    });

                    Log.v("TAG", "Get Data::::" + response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }

    /*private void submitBooking() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat mySdf = new SimpleDateFormat(myFormat, Locale.US);

        final WebServiceHelper mHelper = WebServiceHelper.getInstance(getActivity());
        mHelper.addFormParam(AppConstants.PARAM_MEMBER_ID, userData.getClub_member_id());
        mHelper.addFormParam("ordered_for", "SELF");
        mHelper.addFormParam("order_member_id", userData.getClub_member_id());
        mHelper.addFormParam("category_id", productModel.getCategoryId());
        mHelper.addFormParam(AppConstants.PARAM_PRODUCT_ID, productModel.getId());
        mHelper.addFormParam("booked_from_date", mySdf.format(myCalendar.getTime()));
        mHelper.addFormParam("booked_to_date", mySdf.format(myCalendar.getTime()));
        mHelper.addFormParam("order_comment", "");
        mHelper.addFormParam("max_booking_allowed", productModel.getMaxBookingAllowed());
        mHelper.addFormParam("repeat_event_data", currentSelectedTabs.get(0).getSlotId());
        mHelper.addFormParam("no_of_rooms", "1");
        mHelper.addFormParam("total_price", productModel.getNonMemberPrice());
        mHelper.addFormParam("convenience_fee", "0");
        mHelper.addFormParam("payment_type", "Cash");
        mHelper.addFormParam("created_by", userData.getBusinessObject().get(0).getId().toString());
        mHelper.addFormParam("is_admin", "0");
        mHelper.addFormParam("cheque_card_number", "");
        mHelper.addFormParam("bank_name", "");
        mHelper.addFormParam("coach_form_no", "");

        mHelper.postFormData(AppConstants.URL_SUBMIT_SLOT, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);

                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getActivity(), "Thank you for your booking!", Toast.LENGTH_SHORT).show();
                        Utils.hideProgressbar();
                        ((MainActivity) getActivity()).loadBookingHistory(new BookingHistoryFragment());
                    });

                    Log.v("TAG", "Get Data::::" + response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }*/

}
