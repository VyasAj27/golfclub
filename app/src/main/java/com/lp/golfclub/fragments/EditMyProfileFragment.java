package com.lp.golfclub.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.activities.ResetPasswordActivity;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.model.UserData;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.AppValidator;
import com.lp.golfclub.util.PreferencesHelper;
import com.lp.golfclub.util.Utils;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class EditMyProfileFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.rlGender)
    RelativeLayout rlGender;

    @BindView(R.id.tvResetPass)
    AppCompatTextView tvResetPass;

    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;

    @BindView(R.id.etFirstName)
    AppCompatEditText etFirstName;

    @BindView(R.id.etLastName)
    AppCompatEditText etLastName;

    @BindView(R.id.etEmail)
    AppCompatEditText etEmail;

    @BindView(R.id.etAge)
    AppCompatEditText etAge;

    @BindView(R.id.tvGender)
    AppCompatTextView tvGender;

    @BindView(R.id.tvSubmit)
    AppCompatTextView tvSubmit;

    @BindView(R.id.tvUploadPic)
    AppCompatTextView tvUploadPic;

    @BindView(R.id.etAny)
    AppCompatEditText etAny;

    @BindView(R.id.etMobile)
    AppCompatEditText etMobile;

    @BindView(R.id.tilAny)
    TextInputLayout tilAny;

    @BindView(R.id.pbLoader)
    ProgressBar pbLoader;

    private UserData userData;
    private boolean isPhoto;

    private File selectedPhoto = null;
    private File selectedPhotoId = null;

    public EditMyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void updateUserData() {
        Glide.with(this)
                .load(AppConstants.BASE_URL_IMAGE + userData.getProfileImage())
                .placeholder(R.drawable.ic_user_profile_photo)
                .centerCrop()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        pbLoader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        pbLoader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(ivProfile);
        etMobile.setText(userData.getMobile());
        etEmail.setText(userData.getEmail());
        etAge.setText(userData.getAge());
        etFirstName.setText(userData.getName());
        etLastName.setText(userData.getLastname());
        tvGender.setText(userData.getGender());
        etAny.setText(userData.getIdProof());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                File file = list.get(0);
                try {
                    if (file != null) {
                        file = new Compressor(getActivity()).compressToFile(file);
                        if (isPhoto) {
                            selectedPhoto = file;
                            Glide.with(getActivity())
                                    .load(selectedPhoto)
                                    .placeholder(R.drawable.ic_user_profile_photo)
                                    .centerCrop()
                                    .into(ivProfile);
                        } else {
                            selectedPhotoId = file;
                            etAny.setText(file.getName());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private String generateBase64(File imgFile) {
        if (imgFile.exists() && imgFile.length() > 0) {
            Bitmap bm = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, bOut);
            return Base64.encodeToString(bOut.toByteArray(), Base64.NO_WRAP);
        }
        return null;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.edit_my_profile_fragment, container, false);
        ButterKnife.bind(this, view);

        tvResetPass.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);

        rlGender.setOnClickListener(view1 -> {
            PopupMenu pum = new PopupMenu(getActivity(), rlGender);
            pum.inflate(R.menu.menu_gender);
            pum.setOnMenuItemClickListener(menuItem -> {
                tvGender.setText(menuItem.getTitle());
                return false;
            });
            pum.show();
        });

        etAny.setOnClickListener(v -> tilAny.performClick());
        tvUploadPic.setOnClickListener(v -> ivProfile.performClick());

        ivProfile.setOnClickListener(v -> {
            isPhoto = true;
            EasyImage.openChooserWithGallery(getActivity(), "Capture Photo", 3);
        });
        tilAny.setOnClickListener(v -> {
            isPhoto = false;
            EasyImage.openChooserWithGallery(getActivity(), "Select ID Proof", 3);
        });

        ((MainActivity) getActivity()).showSecondaryToolbar("My Profile");
        userData = (UserData) PreferencesHelper.preferencesHelper.getObject(PreferencesHelper.PREF_USER_DATA, UserData.class);
        updateUserData();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvResetPass:
                Intent mainIntent = new Intent(getActivity(), ResetPasswordActivity.class);
                startActivity(mainIntent);
                break;
            case R.id.tvSubmit:
                if (validate()) {
                    updateProfile();
                }
                break;
        }
    }

    private boolean validate() {
        if (!AppValidator.isEmpty(etFirstName.getText())) {
            Toast.makeText(getActivity(), "Enter valid first name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etLastName.getText())) {
            Toast.makeText(getActivity(), "Enter valid last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (tvGender.getText().toString().equals("Gender")) {
            Toast.makeText(getActivity(), "Select Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!AppValidator.isEmpty(etMobile.getText())) {
            Toast.makeText(getActivity(), "Enter mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etMobile.getText().toString().length() != 10) {
            Toast.makeText(getActivity(), "Mobile number should be of 10 digit.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void updateProfile() {

        getActivity().runOnUiThread(() -> {
            Utils.showProgressbar(getActivity());
        });
        final WebServiceHelper mHelper = WebServiceHelper.getInstance();

        mHelper.addFormParam("user_id", userData.getId().toString());
        mHelper.addFormParam(AppConstants.PARAM_FIRST_NAME, etFirstName.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_LAST_NAME, etLastName.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_EMAIL, etEmail.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_MOBILE, etMobile.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_AGE, etAge.getText().toString().trim());
        mHelper.addFormParam(AppConstants.PARAM_GENDER, tvGender.getText().toString().trim());
        if(selectedPhoto != null)mHelper.addFile(AppConstants.PARAM_PROFILE_IMAGE, selectedPhoto.getName(), selectedPhoto);
        if(selectedPhotoId != null)mHelper.addFile(AppConstants.PARAM_ID_PROOF, selectedPhotoId.getName(), selectedPhotoId);

        mHelper.postFormData(AppConstants.URL_UPDATE_PROFILE, new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {

                if (TextUtils.isEmpty(response)) return;
                final WebResponseModel webResponseModel;
                try {
                    webResponseModel = mHelper.getResponseModel(response);
                    UserData userData = new Gson().fromJson(webResponseModel.getData(), UserData.class);

                    PreferencesHelper.preferencesHelper.putObject(PreferencesHelper.PREF_USER_DATA, userData);

                    getActivity().runOnUiThread(() -> {
                        Utils.hideProgressbar();
                        getFragmentManager().popBackStackImmediate();
                        Toast.makeText(getActivity(), "User profile updated successfully...", Toast.LENGTH_SHORT).show();
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {

                getActivity().runOnUiThread(() -> {
                    Utils.hideProgressbar();
                    Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
                });
            }
        }, WebServiceHelper.METHOD_POST);
    }
}
