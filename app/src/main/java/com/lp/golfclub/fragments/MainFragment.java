package com.lp.golfclub.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.adapter.TutorialAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.rcTutorials)
    RecyclerView rcTutorials;

    @BindView(R.id.containerSlot)
    ConstraintLayout containerSlot;

    @BindView(R.id.rlTerminology)
    RelativeLayout rlTerminology;

    @BindView(R.id.rlGolfRule)
    RelativeLayout rlGolfRule;

    @BindView(R.id.rlHistory)
    RelativeLayout rlHistory;

    @BindView(R.id.clLastSlot)
    ConstraintLayout clLastSlot;

    private TutorialAdapter mAdapter;

    private ArrayList<String> alTutorials;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        initTutorialAdapter();
        initClickListener();
        FirebaseApp.initializeApp(getActivity());
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                // send it to server
                Log.v("FCM TOken" , "FCM Token : "+ token);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showPrimaryToolbar();
    }

    private void initClickListener() {
        containerSlot.setOnClickListener(this);
        rlTerminology.setOnClickListener(this);
        clLastSlot.setOnClickListener(this);
        rlGolfRule.setOnClickListener(this);
        rlHistory.setOnClickListener(this);
    }

    private void initTutorialAdapter() {
        addData();
        mAdapter = new TutorialAdapter(alTutorials, getActivity());
        rcTutorials.setItemAnimator(new DefaultItemAnimator());
        rcTutorials.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rcTutorials.setAdapter(mAdapter);
    }

    private void addData() {
        alTutorials = new ArrayList<>();
        alTutorials.add("");
        alTutorials.add("");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.containerSlot:
                ((MainActivity) getActivity()).loadNewBooking(new ProductListFragment());
                break;
            case R.id.rlTerminology:
                ((MainActivity) getActivity()).loadTerminology(new GolfTerminologyFragment());
                break;
            case R.id.rlGolfRule:
                ((MainActivity) getActivity()).loadBookingHistory(new GolfRulesFragment());
                break;
            case R.id.clLastSlot:
                ((MainActivity) getActivity()).loadBookingHistory(new BookingHistoryFragment());
                break;
            case R.id.rlHistory:
                ((MainActivity) getActivity()).loadBookingHistory(new HistoryOfGolfFragment());
                break;

        }
    }
}
