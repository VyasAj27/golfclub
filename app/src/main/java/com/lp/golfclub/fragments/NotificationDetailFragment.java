package com.lp.golfclub.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.model.NotificationList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
@SuppressLint("ValidFragment")
public class NotificationDetailFragment extends Fragment {

    @BindView(R.id.tvDetail)
    AppCompatTextView tvDetail;

    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;

    @BindView(R.id.tvDate)
    AppCompatTextView tvDate;


    private NotificationList notificationList;

    @SuppressLint("ValidFragment")
    public NotificationDetailFragment(NotificationList notificationList) {
        // Required empty public constructor
        this.notificationList = notificationList;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Notifications");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.notification_detail_fragment, container, false);
        ButterKnife.bind(this, view);

        tvDetail.setMovementMethod(new ScrollingMovementMethod());

        tvDate.setText(notificationList.getTodayDate());
        tvTitle.setText(notificationList.getTitle());
        tvDetail.setText(notificationList.getDescription());

        return view;
    }

}
