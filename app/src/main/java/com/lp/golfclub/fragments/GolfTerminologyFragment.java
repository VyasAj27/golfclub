package com.lp.golfclub.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.golfclub.R;
import com.lp.golfclub.activities.MainActivity;
import com.lp.golfclub.adapter.AlphabatesAdapter;
import com.lp.golfclub.adapter.TerminologyAdapter;
import com.lp.golfclub.api.WebResponseModel;
import com.lp.golfclub.api.WebServiceHelper;
import com.lp.golfclub.interfaces.ClickCallback;
import com.lp.golfclub.model.TerminologyModel;
import com.lp.golfclub.util.AppConstants;
import com.lp.golfclub.util.RecyclerSectionItemDecoration;
import com.lp.golfclub.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class GolfTerminologyFragment extends Fragment implements ClickCallback {


    @BindView(R.id.rvAlphabates)
    RecyclerView rvAlphabates;

    @BindView(R.id.rvTerminology)
    RecyclerView rvTerminology;

    @BindView(R.id.tvSelectedTerm)
    AppCompatTextView tvSelectedTerm;

    @BindView(R.id.ivSearch)
    AppCompatImageView ivSearch;

    @BindView(R.id.etSearch)
    AppCompatEditText etSearch;

    private ArrayList<String> alAlphabates;
    private ArrayList<TerminologyModel> alTerminologies = new ArrayList<>();
    private AlphabatesAdapter mAdapter;
    private TerminologyAdapter terminologyAdapter;

    public GolfTerminologyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSecondaryToolbar("Golf Terminology");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terminology, container, false);
        ButterKnife.bind(this, view);
        initTutorialAdapter();
        initTerminologiesAdapter();
        getTerminology();

        ivSearch.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etSearch.getText())) {
                Toast.makeText(getActivity(), "Type to search..", Toast.LENGTH_SHORT).show();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(etSearch.getText())) {
                    terminologyAdapter.getFilter().filter("");
                    Toast.makeText(getActivity(), "Type to search..", Toast.LENGTH_SHORT).show();
                } else {
                    terminologyAdapter.getFilter().filter(etSearch.getText(), i -> {

                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (TextUtils.isEmpty(etSearch.getText())) {
                    Toast.makeText(getActivity(), "Type to search..", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            return false;
        });
        return view;
    }

    private void initTutorialAdapter() {
        addData();
        mAdapter = new AlphabatesAdapter(alAlphabates, getActivity(), this);
        int numberOfColumns = 13;
        rvAlphabates.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        rvAlphabates.setItemAnimator(new DefaultItemAnimator());
        rvAlphabates.setAdapter(mAdapter);
    }

    private void initTerminologiesAdapter() {
        terminologyAdapter = new TerminologyAdapter(alTerminologies, getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvTerminology.setLayoutManager(layoutManager);
        rvTerminology.setItemAnimator(new DefaultItemAnimator());
        rvTerminology.setAdapter(terminologyAdapter);
        RecyclerSectionItemDecoration sectionItemDecoration =
                new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen.header),
                        true,
                        getSectionCallback(alTerminologies));
        rvTerminology.addItemDecoration(sectionItemDecoration);
    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<TerminologyModel> people) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                return position == 0
                        || people.get(position).getTitle()
                        .charAt(0) != people.get(position - 1).getTitle()
                        .charAt(0);
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                return people.get(position).getTitle()
                        .subSequence(0,
                                1);
            }
        };
    }

    private void addData() {
        alAlphabates = new ArrayList<>();
        alAlphabates.add("A");
        alAlphabates.add("B");
        alAlphabates.add("C");
        alAlphabates.add("D");
        alAlphabates.add("E");
        alAlphabates.add("F");
        alAlphabates.add("G");
        alAlphabates.add("H");
        alAlphabates.add("I");
        alAlphabates.add("J");
        alAlphabates.add("K");
        alAlphabates.add("L");
        alAlphabates.add("M");
        alAlphabates.add("N");
        alAlphabates.add("O");
        alAlphabates.add("P");
        alAlphabates.add("Q");
        alAlphabates.add("R");
        alAlphabates.add("S");
        alAlphabates.add("T");
        alAlphabates.add("U");
        alAlphabates.add("V");
        alAlphabates.add("W");
        alAlphabates.add("X");
        alAlphabates.add("Y");
        alAlphabates.add("Z");
    }

    private void getTerminology() {
        Utils.showProgressbar(getActivity());

        final WebServiceHelper mHelper = WebServiceHelper.getInstance();

        mHelper.post(AppConstants.URL_TERMINOLOGY.trim(), new WebServiceHelper.WebServiceListener() {

            @Override
            public void onSuccess(final String response) {
                Utils.hideProgressbar();

                if (TextUtils.isEmpty(response)) return;
                try {
                    final WebResponseModel webResponseModel = mHelper.getResponseModel(response);

                    JSONObject mJsonObject = new JSONObject(webResponseModel.getData());

                    alTerminologies.clear();

                    JSONObject alpha = mJsonObject.getJSONObject("alphabates");

                    for (int i = 0; i < alAlphabates.size(); i++) {
                        JSONArray charater = alpha.getJSONArray(alAlphabates.get(i));
                        for (int j = 0; j < charater.length(); j++) {
                            JSONObject jsonObject = charater.getJSONObject(j);
                            TerminologyModel terminologyModel = new TerminologyModel();
                            terminologyModel.setAlphabet(alAlphabates.get(i));
                            terminologyModel.setTitle(jsonObject.getString("title"));
                            terminologyModel.setDesc(jsonObject.getString("description"));
                            alTerminologies.add(terminologyModel);
                        }
                        Log.v("TAG", "Single : " + alAlphabates.get(i));
                    }

                    getActivity().runOnUiThread(() -> terminologyAdapter.getFilter().filter(""));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v("TAG", "Get Data::::" + response);
            }

            @Override
            public void onError(final String error) {
                Utils.hideProgressbar();
            }
        }, WebServiceHelper.METHOD_GET);
    }

    @Override
    public void onItemClick(String result) {
        etSearch.setText("");
        tvSelectedTerm.setText("Terminology (" + result + ")");
        terminologyAdapter.getFilter().filter(result);
    }
}
